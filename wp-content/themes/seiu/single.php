<?php
/**
 * The Template for displaying all single posts.
 *
 * @package seiu
 */

get_header(); ?>

<main class="site-content" role="main">
	<?php while ( have_posts() ) : the_post(); 
				$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' ); 
				if($image):
			?>
				<div class="hero" style="background:url(<?php echo $image[0]; ?>) center center; background-size: cover;">
					<div class="container"><h1><?php the_title(); ?></h1></div>
	</div>
	<?php endif; ?>
	<section class="article-inner">
		<article>
				<div class="article-content content-margin-fixer match left-side">
					<div class="container">
						<?php if(!$image): ?>
							<h1><?php the_title(); ?></h1>
						<?php endif; ?>
						<?php the_content(); ?>
					</div>
					
					<?php 
						$images = get_field('top_image_divider');
						$total = count($images);
						if($images):
						?>

						<div class="image-divider total-<?php echo $total; ?>">
							<?php foreach($images AS $img): ?>
							<div class="div-image" style="background:url(<?php echo $img['image']; ?>) center center; background-size: cover;"></div>
							<?php endforeach; ?>
						</div>

					<?php endif; ?>

					<?php

					$middle_content = get_field('middle_content');
					if($middle_content):

					?>

					<div class="container">
					<?php echo $middle_content; ?>
					</div>

					<?php endif; ?>


					<?php 
						$images = get_field('middle_image_divider');
						$total = count($images);
						if($images):
						?>

						<div class="image-divider total-<?php echo $total; ?>">
							<?php foreach($images AS $img): ?>
							<div class="div-image" style="background:url(<?php echo $img['image']; ?>) center center; background-size: cover;"></div>
							<?php endforeach; ?>
						</div>

					<?php endif; ?>

					<?php

					$lower_content = get_field('lower_content');
					if($lower_content):

					?>

					<div class="container">
						<?php echo $lower_content; ?>
					</div>

					<?php endif; ?>

					<?php 
						$images = get_field('lower_image_divider');
						$total = count($images);
						if($images):
						?>

						<div class="image-divider total-<?php echo $total; ?>">
							<?php foreach($images AS $img): ?>
							<div class="div-image" style="background:url(<?php echo $img['image']; ?>) center center; background-size: cover;"></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>

					<div class="container">
						<p>Next Article:</P>
						<?php next_post_link(); ?>
					</div>

				</div>

		</article>
		<?php include('content-sidebar.php'); ?>
	</section>

<?php endwhile; // end of the loop. ?>
</main><!-- #primary -->

<?php get_footer(); ?>