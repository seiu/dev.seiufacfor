<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package seiu
 */

get_header(); 
$pagedata = seiu_parse_blog();
?>

<main class="site-content" role="main">

					<?php if ( have_posts() ) : ?>

					<section class="news">

						<div class="news-list match left-side">
							<div class="container content-margin-fixer">
						<h1>
							<?php
								if ( is_category() ) :
									echo 'Category: ' . single_cat_title( '', false );

								elseif ( is_tag() ) :
									echo 'Tag: ' . single_tag_title( '', false );

								elseif ( is_author() ) :
									/* Queue the first post, that way we know
									* what author we're dealing with (if that is the case).
									*/
									the_post();
									echo 'Author: ' . get_the_author( '', false );

									/* Since we called the_post() above, we need to
									* rewind the loop back to the beginning that way
									* we can run the loop properly, in full.
									*/
									rewind_posts();

								else :
									echo 'News &amp; Updates';

								endif;
							?>
						</h1>
					<?php while ( have_posts() ) : the_post(); 
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

					?>
								<div class="news-item">
									<?php if($image): ?>
									<div class="image" style="background:url(<?php echo $image[0]; ?>) no-repeat center center; background-size:cover;"><a href="<?php echo get_permalink($post->ID); ?>" class="overlink"></a></div>
									<?php endif; ?>

									<div class="news-blurb <?php echo $image ? '' : 'no-image'; ?>">
										<div class="date"><?php echo date("F j, Y", strtotime($post->post_date)); ?></div>
										<h2><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
										<p><?php echo seiu_truncate(strip_tags($post->post_content), 140); ?></p>
										<a href="<?php echo get_permalink($post->ID); ?>" class="read-more">Read More</a>
									</div>
								</div>
					<?php endwhile; ?>
							</div>
									<?php echo seiu_paginate($pagedata['page'], 10, $pagedata['type'], $pagedata['category']); ?>

						</div>
					<?php $victories = true; include('content-sidebar.php'); ?>
					</section>
		

					<?php else : ?>

						<?php get_template_part( 'no-results', 'archive' ); ?>

					<?php endif; ?>

</main>

<?php get_footer(); ?>