<?php
/**
 * Template Name: Home
 *
 * @package seiu
 */

get_header();
$pagedata = seiu_parse_blog();
?>

<script src='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.js'></script>
<link href='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.css' rel='stylesheet' />
<!--script src='/wp-content/themes/seiu/public_assets/js/data.js'></script-->
<main class="site-content" role="main">
<div id="fb-root"></div>

<?php 

while ( have_posts() ) : 
the_post(); 
    $d = array();
    foreach(get_field('map_points') AS $p):
    	$desc = trim(seiu_truncate($p['description'], 150, true));
    	if($p['link_url'] == '')
    	{
    		$desc = trim($p['description']);
    	}
        $d[] = array(
            "type" => "Feature",
            "properties" => array(
                "title" => $p['title'],
                "subtitle" => $p['subtitle'],
                "image" => $p['image'],
                "description" => $desc,
                "link_url" => $p['link_url'],
                "type" => $p['type'],
                "auto" => $p['auto_popup'] == 'yes' ? 'yes' : 'no'         
            ),
            "geometry" => array(
                "coordinates" => array(
                    $p['longitude'],
                    $p['latitude']
                ),
                "type" => "Point"
            ),
            "id" =>  md5(rand(0,10000).time())
        );

    endforeach;
?>
<script type="text/javascript">
var data = <?php echo json_encode($d); ?>;
</script>
	<section class="slideshow">
		<div class="slideshow-container">
			<?php 
			$i = 0;
			foreach(get_field('slideshow') AS $s): 
				$image = $s['image'];
			?>
			<div id="slide-<?php echo $i; ?>" class="slide <?php echo $i == 0 ? 'active' : ''; ?>" style="background:url(<?php echo $image; ?>) no-repeat center center; background-size:cover;"></div>
			<?php $i++; endforeach; ?>
		</div>
		<div class="overlay-text">
			<div class="overlay-content">
				<?php echo get_field('slideshow_text'); ?>
				<?php if(get_field('slideshow_cta_url')): ?>
					<a href="<?php echo get_field('slideshow_cta_url'); ?>" class="blue-button"><?php echo get_field('slideshow_cta_text'); ?></a>
				<?php endif; ?>
			</div>
			<?php 
				$url = get_field('slideshow_video_url');
				if($url != '')
				{
					if(strpos($url, 'youtu') > -1)
					{
						preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);

						$url = '//www.youtube.com/embed/'.$matches[1].'?rel=0&autoplay=1';
					}
					else
					{
						$url = explode("/", $url);
						$url = '//player.vimeo.com/video/' . end($url);
					}
				}

			if($url != ''):?>
			<div class="video-play" data-url="<?php echo $url; ?>"></div>
			<?php endif; ?>
		</div>
		<!--
        <a href="#" id="prev" class="slideshow-nav"><img src="/wp-content/themes/seiu/public_assets/images/slideshow-prev-arrow.png"></a>
        <a href="#" id="next" class="slideshow-nav"><img src="/wp-content/themes/seiu/public_assets/images/slideshow-next-arrow.png"></a>
		<ul class="dots">

		</ul>
	-->
	</section>
	<div class="close-wrapper">
		<span class="video-close">close video</span>
	</div>
	<section class="home-intro">
		<div class="container">
			<?php the_content(); ?>

			<div class="signup">
				<div id="short-signup-errors"></div>
				<form id="short-signup-form" action="" method="post">
					<label for="join-email">Join The Movement</label>
					<input type="text" id="join-email" name="join-email" placeholder="Email Address">
					<input type="text" id="join-zip" name="join-zip" placeholder="Zip Code">
					
					<div id="short-submit">
						<input type="submit" name="submit" value="Join">
					</div>
				</form>
			</div>
		</div>
	</section>

	<section id="map-area">
		<h2><?php echo get_field('map_header'); ?></h2>
		<div id="map">

		</div>
		<div class="legend">
		  <strong>Map Legend</strong>
		  <div class="grid-colors">
		  	<?php foreach(get_field('map_legends') AS $m): ?>
		    <div class="column" style="background:<?php echo $m['color']; ?>"></div>
			<?php endforeach; ?>
		  </div>
		  <div class="grid-text">
		  	<?php foreach(get_field('map_legends') AS $m): ?>
		    <div class="column"><?php echo $m['title']; ?></div>
			<?php endforeach; ?>
		  </div>

		  <div class="legend-dots">
		  	<?php foreach(get_field('map_legends') AS $m): ?>
		    <div class="dot-item"><div class="dot" style="background:<?php echo $m['color']; ?>"></div><?php echo $m['title']; ?></div>
			<?php endforeach; ?>
		  </div>

		  <a href="http://victories.seiufacultyforward.org/" class="legend-more blue-button" target="_blank">Learn More</a>

		</div>
	</div>

	<?php
	$mvh = get_field('mobile_victories_copy');
	if($mvh):
	?>
	<div class="mobile-more">
		<?php echo $mvh; ?>
		<a href="http://victories.seiufacultyforward.org/" class="blue-button" target="_blank">Learn More</a>
	</div>
	<?php endif; ?>

	<section id="voices">
		<h2><?php echo get_field('members_header'); ?></h2>
		<div class="voice-slider">
			<?php $i = 0; foreach(get_field('members') AS $m): ?>
			<div id="voice-<?php echo $i; ?>" class="voice <?php echo $i > 1 ? 'hide-mobile' : ''; ?>" style="background:url(<?php echo $m['image']; ?>) no-repeat center center; background-size:cover;">
				<img src="<?php echo $m['image']; ?>" alt="" class="mobile-image">
				<div class="voice-overlay" data-i="<?php echo $i; ?>" data-image="<?php echo $m['image']; ?>">
					<div class="voice-short"><?php echo seiu_truncate($m['copy'], 260); ?>...</div>
					<div class="voice-long"><?php echo $m['copy']; ?></div>
					<span class="voice-name"><?php echo $m['name']; ?></span>
				</div>
			</div>
			<?php $i ++; endforeach; ?>
		</div>
		<span class="vm-left"></span>
		<span class="vm-right"></span>
	</section>
	<div id="voice-popup">
		<div id="voice-popup-wrapper">
			<div id="voice-popup-contents">
			</div>
			<span class="voice-prev"></span>
			<span class="voice-next"></span>
			<span class="voice-close">X</span>
		</div>
		<div class="popup-trigger"></div>
	</div>

  <?php endwhile; // end of the loop. ?>

  <section class="news">
  	<div class="news-list match left-side">
		  <div class="content-margin-fixer">
			<?php

				$per = 3;
						$offset = ($pagedata['page'] * $per) - $per;

						$args = array(
							'post_type' => 'post',
							'posts_per_page' => $per,
							'offset' => $offset
						);

						if($pagedata['category'] != '')
						{
							$args['category_name'] = $pagedata['category'];
						}

			$news = new WP_Query($args);
			if($news->have_posts()):
			while ( $news->have_posts() ):
				$news->the_post();
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
			?>
			<div class="news-item">
				<?php if($image): ?>
					<div class="image" style="background:url(<?php echo $image[0]; ?>) no-repeat center center; background-size:cover;"><a href="<?php echo get_permalink($post->ID); ?>" class="overlink"></a></div>
					<?php endif; ?>
				<div class="news-blurb <?php echo $image ? '' : 'no-image'; ?>">
					<div class="date"><?php echo date("F j, Y", strtotime($post->post_date)); ?></div>
					<h2><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
					<p><?php echo seiu_truncate(strip_tags($post->post_content), 140); ?></p>
					<a href="<?php echo get_permalink($post->ID); ?>" class="read-more">Read More</a>
				</div>
			</div>
			<?php endwhile; ?>


			<?php echo seiu_paginate($pagedata['page'], $per, $pagedata['type'], $pagedata['category']); ?>
			<?php endif;?>
		</div>
  	</div>
  <?php 
	$sidebar_image = get_field('sidebar_image');
	$sidebar_cta_label = get_field('sidebar_cta_label');
	$sidebar_cta_link_url = get_field('sidebar_cta_link_url');
	include('content-sidebar.php'); 
  ?>
  </section>


</main>

<?php get_footer(); ?>
