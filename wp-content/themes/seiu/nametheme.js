var fs = require('fs'),
    path = require('path'),
    requirestream = require('stream');

var THEMENAME = process.argv.slice(2),
    BASENAME = 'brink_starter';

function isPhpOrCss(filename) {
  // check if file has .php extension;
  // return true or false
  var ext = path.extname(filename);
  return (ext === '.php' || ext === '.css') ? true : false;
}

// patterns laid out by _s here:
// https://github.com/Automattic/_s/#getting-started
function pattern1(str) {
  return '\'' + str + '\'';
}
function pattern2(str) {
  return str + '_';
}
function pattern3(str) {
  return ' ' + str;
}
function pattern4(str) {
  return str + '-';
}

function replaceName(filename) {
  // regex-replace BASENAME parts with THEMENAME
  var tn = THEMENAME,
      bn = BASENAME,
      output;

  fs.readFile(filename, { encoding: 'utf8' }, function(err, data) {
    if (err) { console.log(err); }

    data = data.replace(new RegExp(pattern1(bn), 'g'), pattern1(tn));
    data = data.replace(new RegExp(pattern2(bn), 'g'), pattern2(tn));
    data = data.replace(new RegExp(pattern3(bn), 'g'), pattern3(tn));
    data = data.replace(new RegExp(pattern4(bn), 'g'), pattern4(tn));

    output = fs.createWriteStream(filename);
    output.write(data);
  });
}

function loopFiles(err, files) {
  // loop through files
  if (err) { console.log(err); }
  for (var i=0,l=files.length;i<l;i++) {
    var filename = files[i];
    if (isPhpOrCss(filename)) {
      replaceName(filename);
    }
  }
}

// do it
fs.readdir('.', loopFiles);