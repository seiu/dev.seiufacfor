
var data = [
    {
     "type": "Feature",
     "properties": {
       "title": "University of Arizona",
       "subtitle": "Graduate Students Union Formed: April 4th, 2017",
       "image": "http://dev.seiu.com/wp-content/uploads/2015/09/1.jpg",
       "description": "Lorem ipsum dolor set amit and a bunch more text that can show in the popup. This is a short blurb",
       "type": "faculty"
     },
     "geometry": {
       "coordinates": [
         -110.950265,
         32.231692
       ],
       "type": "Point"
     },
     "id": "6f03267587cec623548bcb2991746d72"
   },
   {
     "type": "Feature",
     "properties": {
       "title": "Lipsum",
       "subtitle": "A school since 1922",
       "image": "http://dev.seiu.com/wp-content/uploads/2015/09/1.jpg",
       "description": "This is another school for fun",
       "type": "adjunct"
     },
     "geometry": {
       "coordinates": [
         -77.959433,
         37.580478
       ],
       "type": "Point"
     },
     "id": "8fbf32a2ace7d29097d82dc16e0f1499"
   },
   {
     "type": "Feature",
     "properties": {
       "title": "A northern School",
       "subtitle": "some more subtitles",
       "image": "http://dev.seiu.com/wp-content/uploads/2015/09/1.jpg",
       "description": "lorem ipsum dolor set amit",
       "type": "graduate"
     },
     "geometry": {
       "coordinates": [
         -99.271369,
         44.414907
       ],
       "type": "Point"
     },
     "id": "cc869ae4bce4b3360cab99bc54fa93a7"
   }
  ];