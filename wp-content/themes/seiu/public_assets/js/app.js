(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
jQuery(document).ready(function($){
	var w = 0;
	var pos = 0;
	window.sliderQueue = null;

	$(window).on('scroll', scroll);
	$(window).on('resize', resize);
	setTimeout(function(){
		resize();
		scroll();

		if($('.slideshow-container').length){
			slideshow.init();
		}

		if($('.voice-slider').length){
			voice.init();
		}
	}, 500);

	function resize(){
		w = $(window).width();

		if($('.match').length){
			var max = 0;
			var left = false;
			$('.match').css('height', 'auto');
			if(window.matchMedia("(min-width: 1024px)").matches){
					$('.match').each(function(){
					if($(this).outerHeight() > max){
						if($(this).hasClass('left-side'))
							left = true;

						max = $(this).outerHeight();
					}
				});
				
				$('.match').css('height', max + (left ? 0 : 400));
			}
		}

		if($('.voice-slider').length){
			voice.init();
		}

		if($('.form-left').length)
		{
			$('.form-left, .form-right').css({minHeight:0});
			if(window.matchMedia("(min-width: 1024px)").matches){
				var fl = $('.form-left').outerHeight();
				var fr = $('.form-right').outerHeight();
				if(fl < fr)
				{
					$('.form-left, .form-right').css({minHeight:fr});
				}
				else
				{
					$('.form-left, .form-right').css({minHeight:fl});
				}
			}
		}
	}

	var autolist = [];
	window.autotrigger = false;
	window.autoqueue = null;

	function shuffle(a) {
	    var j, x, i;
	    for (i = a.length; i; i--) {
	        j = Math.floor(Math.random() * i);
	        x = a[i - 1];
	        a[i - 1] = a[j];
	        a[j] = x;
	    }
	}

	function showPopupQueue(){
		var item = autolist.pop();
		window.autoqueue = setTimeout(function(){
			item.click();

			if(autolist.length > 0)
			{
				window.autoqueue = setTimeout(showPopupQueue, 5000);				
			}
		}, 1000);
	}

	function scroll(){
		pos = $(window).scrollTop();

		if($('#map').length && (pos > $('#map').offset().top - 100) && !window.autotrigger){
			window.autotrigger = true;
//.marker-auto
			if($('.marker-auto').length)
			{
				$('.marker-auto').each(function(){
					autolist.push($(this));
				});

				//shuffle(autolist);
				
				showPopupQueue();
			}
		}
	}

	$('#hamburger').on('click', function(){
		$(this).toggleClass('active');
		$('.site-nav-c').toggleClass('active');
	});

	$('#short-submit').on('click', function(e){
		e.preventDefault();

		var email = $('#join-email').val();
		var zip = $('#join-zip').val();

		if(email != '' && zip != '')
		{
			var data = {action: 'signup_short', email:email, zip:zip};
			$.ajax(
		    {
		        type: "post",
		        dataType: "json",
		        url: seiu_ajax.ajax_url,
		        data: data,
		        success: function(msg){
		            if(msg.status == 'success')
		            {
		            	$('#short-signup-form').slideUp();
		            	$('#short-signup-errors').html(msg.message).addClass('success').slideDown();
		            }
		            else
		            {
		            	$('#short-signup-errors').html(msg.message).removeClass('success').slideDown();
		            }
		        }
		    });	
		}
		else
		{
			$('#short-signup-errors').html('Please enter a valid email address and zip code.').removeClass('success').slideDown();
		}
	});

	$('.accordion li h3').on('click', function(){
		var par = $(this).parent();

		if($(this).parent().hasClass('active'))
		{
			par.find('.answer').slideUp(500, function(){
				$(par).removeClass('active');
			});
		}
		else
		{
			$('.accordion li.active .answer').slideUp(500, function(){
				$(this).parent().removeClass('active');
			});
			$(this).siblings('.answer').slideDown(500, function(){
				$(par).addClass('active');
				$('html,body').animate({scrollTop:$(par).offset().top - 50}, 500);
			});
		}
	});

	$('.site-nav-c ul >li.menu-item-has-children >a').on('click', function(e){
		e.preventDefault();
	});

	$('.s-trigger').on('click', function(){
		$(this).fadeOut();
		$('#search').fadeIn();
		$('.s-close').fadeIn();

		if(window.matchMedia("(min-width: 1024px)").matches){
			$('.site-nav-c').addClass('moving');
		}

	});

	$('.s-close').on('click', function(){
		$('.s-trigger').fadeIn();
		$('#search').fadeOut();
		$(this).fadeOut();

		if(window.matchMedia("(min-width: 1024px)").matches){
			$('.site-nav-c').removeClass('moving');
		}
	});

	$('.voice-overlay').on('click', function(){
		setPopup($(this).data('i'));
	});

	$('.voice-close, .popup-trigger').on('click', function(){
		$('#voice-popup-contents').html('');
		$('#voice-popup').fadeOut();
	});

	$('.voice-prev').on('click', function(){
		var cur = parseInt($('#voice-popup-contents').data('cur'));
		cur -= 1;
		if(cur < 0)
		{
			cur = $('.voice').length - 1;
		}

		setPopup(cur);
	});

	$('.voice-next').on('click', function(){
		var cur = parseInt($('#voice-popup-contents').data('cur'));
		cur += 1;
		if(cur > $('.voice').length - 1)
		{
			cur = 0;
		}

		setPopup(cur);
	});

	$('.vm-left').on('click', function(){
		voice.slide('prev');
	});

	$('.vm-right').on('click', function(){
		voice.slide('next');
	});

	function setPopup(i){
		if(voice.viewable <= 1)
			return;

		$('#voice-popup-contents').data('cur', i);
		$('#voice-popup-contents .popup-inner').addClass('transition');

		var image = $('#voice-' + i + ' .voice-overlay').data('image');
		var cont = $('<div>').addClass('popup-inner new');
		var im = cont.append($('<img>').attr('src', image));
		var copy = cont.append($('#voice-' + i + ' .voice-overlay').html());

		$('#voice-popup-contents').append(cont);

		if($('#voice-popup').is(':visible'))
		{
			cont.fadeIn(500, function(){
				$('.popup-inner.transition').remove();
				cont.removeClass('new');
			});
		}
		else
		{
			cont.css('display', 'block');
			$('#voice-popup').fadeIn();			
		}

	}

  if($('#map').length)
  {

    L.mapbox.accessToken = 'pk.eyJ1IjoiYnJpbmttZWRpYSIsImEiOiJjancwdjZpOTcwZmgyNGJ0Z2M2Z2tpN2Z0In0.L1i5Yb7VVgDtGjwqSRE9QA';
    var bounds = [
    	[40.8, -120], 
    	[41.5, -80]
    ];
    var mapTwo = L.mapbox.map('map').setView([40.8, -100], 5);
    mapTwo.fitBounds(bounds);

    L.mapbox.styleLayer('mapbox://styles/brinkmedia/cj13sm02r00052rp1alsfjdez').addTo(mapTwo);
    var myLayer = L.mapbox.featureLayer().addTo(mapTwo);

    myLayer.on('layeradd', function(e) {
		var marker = e.layer,
		feature = marker.feature;

		marker.setIcon(L.divIcon({
          className: feature.properties.type, 
          iconSize: null 
        }));


		var popupContent =  '<div class="popup' + (feature.properties.image == '' ? ' no-image' : '') + '">' +
							(feature.properties.image != '' ? '<div class="popup-image"><img src="' + feature.properties.image + '" alt="">' : '') +
                            '<h2>' + feature.properties.title + '</h2>' +
                            '<h3>' + feature.properties.subtitle + '</h3>';

        if(feature.properties.description != ''){
        	popupContent += '<p>' + feature.properties.description + '</p>';
        }

        if(feature.properties.link_url != ''){
        	popupContent += '<p><a href="' + feature.properties.link_url + '" target="_blank" class="popup-read-more">Read More</a></p>'
        }
        
        popupContent += '</div><span class="popup-close">X</span></div>';


		marker.bindPopup(popupContent,{
			closeButton: false,
			minWidth: 320
		});

		if(feature.properties.auto == 'yes')
		{
			$(marker._icon).addClass('marker-auto');
		}

		marker.on('popupclose', function(e) {
		    $(marker._icon).removeClass('selectedMarker');
		});

		marker.on('click', function() {
		    $(marker._icon).addClass('selectedMarker');
		    var ll = this.getLatLng();
		    var cz = mapTwo.getZoom();
		    var nll;
		    if(cz == 5)
		    {
				nll = {
			    	lat: (ll.lat + 5),
			    	lng: ll.lng
			    };
		    }
		    else if(cz == 6)
		    {
				nll = {
			    	lat: (ll.lat + 3),
			    	lng: ll.lng
			    };
		    }
		    else
		    {
				nll = {
			    	lat: (ll.lat + 1),
			    	lng: ll.lng
			    };
		    }

		    mapTwo.panTo(nll);
		    //console.log(cz, nll);
		    clearTimeout(window.autoqueue);
		});
    });

    myLayer.setGeoJSON(data);
    if (mapTwo.scrollWheelZoom) {
      mapTwo.scrollWheelZoom.disable();
    }

    $(document).on('click', '.popup-close', function(){
    	mapTwo.closePopup();
    	$('.leaflet-marker-icon').removeClass('selectedMarker');
    });
  }

  	var voice = {
  		cur:0,
  		cw: 0,
  		viewable: 0,
  		total: 0,
  		init: function(){
  			voice.cur = 0;
  			voice.cw = w;
  			voice.viewable = 1;
  			voice.total = $('.voice').length;

  			if(window.matchMedia("(min-width: 783px)").matches)
  			{
  				voice.cw = w / 2;
  				voice.viewable = 2;
  			}
  			if(window.matchMedia("(min-width: 1024px)").matches)
  			{
  				voice.cw = w / 3;
  				voice.viewable = 3;
  			}
  			if(window.matchMedia("(min-width: 1450px)").matches)
  			{
				voice.cw = w / 4;
				voice.viewable = 4;
  			}

  			$('.voice').css({width:voice.cw});

  			if(voice.total <= voice.viewable || voice.viewable <= 1)
  			{
  				$('.vm-left, .vm-right').hide();
  			}
  			else
  			{
				$('.vm-left, .vm-right').show();
  			}

  			$('.voice-slider').css({width: (voice.total + 2) * voice.cw, marginLeft:0 });
  		},
  		slide: function(dir){
  			var left = voice.total - voice.cur;

  			if(dir == 'next')
  			{
  				voice.cur += 1;

  				if(left <= voice.viewable)
  				{
  					console.log(voice.total - voice.viewable, voice.cur, ((voice.total - voice.viewable) * voice.cw) * -1);
  					$('.voice-slider').append($('.voice').first().detach()).css({marginLeft: (((voice.total - voice.viewable) - 1) * voice.cw) * -1});
  					cycle = true;
  					voice.cur -= 1;
  				}

  				$('.voice-slider').animate({marginLeft: (voice.cur * voice.cw) * -1}, 500);
  			}
  			else
  			{
  				voice.cur -= 1;
  				if(voice.cur < 0)
  				{
  					$('.voice-slider').prepend($('.voice').last().detach()).css({marginLeft:voice.cw * -1});
  					voice.cur = 0;
  				}

  				$('.voice-slider').animate({marginLeft: voice.cur * voice.cw * -1}, 500);
  			}
  		}
  	};

	var slideshow = {
		cur:0,
		delay: 5000,
		autoscroll:false,
		queue: null,
		moving:false,
		dotClick: null,
		init: function(){
			if($('.slide').length > 1)
			{
				var li;
				for(var n = 0; n < $('.slide').length; n ++)
				{
					li = $('<li>').addClass('dot').attr('data-slide', n).attr('id', 'dot-' + n);
					if(n == 0)
						li.addClass('active');

					li.appendTo($('.dots'));
				}
			}
			else
			{
				$('.slideshow-nav').hide();
			}
			slideshow.beginSlide();
		},
		scroll:function(dir){
			if(slideshow.moving)
				return false;

			slideshow.moving = true;
			var box, tween, bout;
			if(dir == 'next')
			{
				bout = $('#slide-' + slideshow.cur);
				/*
				tween = TweenLite.to(bout, 1, {
					 x: $(window).outerWidth() * -1,
					 ease: Power1.easeInOut,
					 delay: .2,
					 onComplete: function(){
						bout.removeClass('active');
					}
				});
				*/

				if(slideshow.dotClick != null)
				{
					slideshow.cur = slideshow.dotClick;
					slideshow.dotClick = null;
				}
				else
				{
					slideshow.cur += 1;
				}

				if(slideshow.cur > $('.slide').length - 1)
				{
					slideshow.cur = 0;
				}

				/*
				tween = TweenLite.to($('#slide-' + slideshow.cur), 0, {
					 x: $(window).outerWidth(),
					 delay: 0,
					 onComplete: function(){
						$('#slide-' + slideshow.cur).addClass('active');
					}
				});
				*/
			}
			else
			{
				bout = $('#slide-' + slideshow.cur);
				/*
				tween = TweenLite.to(bout, 1, {
					 x: $(window).outerWidth(),
					 ease: Power1.easeInOut,
					 delay: .2,
					 onComplete: function(){
						bout.removeClass('active');
					}
				});
				*/

				if(slideshow.dotClick != null)
				{
					slideshow.cur = slideshow.dotClick;
					slideshow.dotClick = null;
				}
				else
				{
					slideshow.cur -= 1;
				}

				if(slideshow.cur < 0)
				{
					slideshow.cur = $('.slide').length - 1;
				}
				/*
				tween = TweenLite.to($('#slide-' + slideshow.cur), 0, {
					 x: $(window).outerWidth() * -1,
					 delay: 0,
					 onComplete: function(){
						$('#slide-' + slideshow.cur).addClass('active');
					}
				});
				*/

			}

			bout.css({zIndex:1});
			$('#slide-' + slideshow.cur).css({zIndex:2, opacity:0}).animate({opacity:1}, 1000, function(){
				$('#slide-' + slideshow.cur).addClass('active').css({zIndex:1});
				bout.removeClass('active').css({opacity:0, zIndex:0});

				if(slideshow.autoscroll)
				{
					slideshow.queue = setTimeout(function(){
						slideshow.scroll('next');
					}, slideshow.delay);
				}

				slideshow.moving = false;

			});

			box = $('#slide-' + slideshow.cur);
			$('.dot.active').removeClass('active');
			$('#dot-' + slideshow.cur).addClass('active');
			// Create a tween
			/*
			tween = TweenLite.to(box, 1, {
			 x: 0,
			 ease: Power1.easeInOut,
			 delay: .2,
			 onComplete: function(){
				if(slideshow.autoscroll)
				{
					slideshow.queue = setTimeout(function(){
						slideshow.scroll('next');
					}, slideshow.delay);
				}

				slideshow.moving = false;
			}
			});*/
		},
		beginSlide: function(){
			slideshow.autoscroll = true;
			slideshow.queue = setTimeout(function(){
				slideshow.scroll('next');
			}, slideshow.delay);
		}
	};

	$('.slideshow-nav').on('click', function(e){
		e.preventDefault();

		slideshow.autoscroll = false;
		clearTimeout(slideshow.queue);
		slideshow.scroll($(this).attr('id'));
		$('.video-overlay').remove();
		$('.overlay-text').fadeIn();
	});

	$(document).on('click', '.dot', function(e){
		e.preventDefault();

		slideshow.dotClick = $(this).data('slide');

		if(slideshow.dotClick == slideshow.cur)
		{
			slideshow.dotClick = null;
			return;
		}


		slideshow.autoscroll = false;
		clearTimeout(slideshow.queue);
		$('.video-overlay').remove();
		$('.overlay-text').fadeIn();
		if(slideshow.dotClick < slideshow.cur)
			slideshow.scroll('prev');
		else
			slideshow.scroll('next');
	});

	$('.video-play').on('click', function(){
		clearInterval(slideshow.queue);
		var vid = $(this).data('url');
		$('.overlay-text').fadeOut();
		$(this).parent().parent().append($('<div>').addClass('video-overlay').html('<div class="videoWrapper"><iframe src="' + vid + '" frameborder="0" allowfullscreen></iframe></div>'));
		$('.video-close').slideDown();
	});

	$('.video-close').on('click', function(){
		$('.video-overlay').remove();
		$('.overlay-text').fadeIn();
		$('.video-close').slideUp();
		slideshow.init();
	});
});
},{}]},{},[1])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9BcHBsaWNhdGlvbnMvTUFNUC9odGRvY3MvYmVybGluLXNlaXUtbGl2ZS93cC1jb250ZW50L3RoZW1lcy9zZWl1L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvQXBwbGljYXRpb25zL01BTVAvaHRkb2NzL2Jlcmxpbi1zZWl1LWxpdmUvd3AtY29udGVudC90aGVtZXMvc2VpdS9zcmMvanMvbWFpbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpfXZhciBmPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChmLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGYsZi5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJqUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCQpe1xuXHR2YXIgdyA9IDA7XG5cdHZhciBwb3MgPSAwO1xuXHR3aW5kb3cuc2xpZGVyUXVldWUgPSBudWxsO1xuXG5cdCQod2luZG93KS5vbignc2Nyb2xsJywgc2Nyb2xsKTtcblx0JCh3aW5kb3cpLm9uKCdyZXNpemUnLCByZXNpemUpO1xuXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cdFx0cmVzaXplKCk7XG5cdFx0c2Nyb2xsKCk7XG5cblx0XHRpZigkKCcuc2xpZGVzaG93LWNvbnRhaW5lcicpLmxlbmd0aCl7XG5cdFx0XHRzbGlkZXNob3cuaW5pdCgpO1xuXHRcdH1cblxuXHRcdGlmKCQoJy52b2ljZS1zbGlkZXInKS5sZW5ndGgpe1xuXHRcdFx0dm9pY2UuaW5pdCgpO1xuXHRcdH1cblx0fSwgNTAwKTtcblxuXHRmdW5jdGlvbiByZXNpemUoKXtcblx0XHR3ID0gJCh3aW5kb3cpLndpZHRoKCk7XG5cblx0XHRpZigkKCcubWF0Y2gnKS5sZW5ndGgpe1xuXHRcdFx0dmFyIG1heCA9IDA7XG5cdFx0XHR2YXIgbGVmdCA9IGZhbHNlO1xuXHRcdFx0JCgnLm1hdGNoJykuY3NzKCdoZWlnaHQnLCAnYXV0bycpO1xuXHRcdFx0aWYod2luZG93Lm1hdGNoTWVkaWEoXCIobWluLXdpZHRoOiAxMDI0cHgpXCIpLm1hdGNoZXMpe1xuXHRcdFx0XHRcdCQoJy5tYXRjaCcpLmVhY2goZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRpZigkKHRoaXMpLm91dGVySGVpZ2h0KCkgPiBtYXgpe1xuXHRcdFx0XHRcdFx0aWYoJCh0aGlzKS5oYXNDbGFzcygnbGVmdC1zaWRlJykpXG5cdFx0XHRcdFx0XHRcdGxlZnQgPSB0cnVlO1xuXG5cdFx0XHRcdFx0XHRtYXggPSAkKHRoaXMpLm91dGVySGVpZ2h0KCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdCQoJy5tYXRjaCcpLmNzcygnaGVpZ2h0JywgbWF4ICsgKGxlZnQgPyAwIDogNDAwKSk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0aWYoJCgnLnZvaWNlLXNsaWRlcicpLmxlbmd0aCl7XG5cdFx0XHR2b2ljZS5pbml0KCk7XG5cdFx0fVxuXG5cdFx0aWYoJCgnLmZvcm0tbGVmdCcpLmxlbmd0aClcblx0XHR7XG5cdFx0XHQkKCcuZm9ybS1sZWZ0LCAuZm9ybS1yaWdodCcpLmNzcyh7bWluSGVpZ2h0OjB9KTtcblx0XHRcdGlmKHdpbmRvdy5tYXRjaE1lZGlhKFwiKG1pbi13aWR0aDogMTAyNHB4KVwiKS5tYXRjaGVzKXtcblx0XHRcdFx0dmFyIGZsID0gJCgnLmZvcm0tbGVmdCcpLm91dGVySGVpZ2h0KCk7XG5cdFx0XHRcdHZhciBmciA9ICQoJy5mb3JtLXJpZ2h0Jykub3V0ZXJIZWlnaHQoKTtcblx0XHRcdFx0aWYoZmwgPCBmcilcblx0XHRcdFx0e1xuXHRcdFx0XHRcdCQoJy5mb3JtLWxlZnQsIC5mb3JtLXJpZ2h0JykuY3NzKHttaW5IZWlnaHQ6ZnJ9KTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdHtcblx0XHRcdFx0XHQkKCcuZm9ybS1sZWZ0LCAuZm9ybS1yaWdodCcpLmNzcyh7bWluSGVpZ2h0OmZsfSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHR2YXIgYXV0b2xpc3QgPSBbXTtcblx0d2luZG93LmF1dG90cmlnZ2VyID0gZmFsc2U7XG5cdHdpbmRvdy5hdXRvcXVldWUgPSBudWxsO1xuXG5cdGZ1bmN0aW9uIHNodWZmbGUoYSkge1xuXHQgICAgdmFyIGosIHgsIGk7XG5cdCAgICBmb3IgKGkgPSBhLmxlbmd0aDsgaTsgaS0tKSB7XG5cdCAgICAgICAgaiA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIGkpO1xuXHQgICAgICAgIHggPSBhW2kgLSAxXTtcblx0ICAgICAgICBhW2kgLSAxXSA9IGFbal07XG5cdCAgICAgICAgYVtqXSA9IHg7XG5cdCAgICB9XG5cdH1cblxuXHRmdW5jdGlvbiBzaG93UG9wdXBRdWV1ZSgpe1xuXHRcdHZhciBpdGVtID0gYXV0b2xpc3QucG9wKCk7XG5cdFx0d2luZG93LmF1dG9xdWV1ZSA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblx0XHRcdGl0ZW0uY2xpY2soKTtcblxuXHRcdFx0aWYoYXV0b2xpc3QubGVuZ3RoID4gMClcblx0XHRcdHtcblx0XHRcdFx0d2luZG93LmF1dG9xdWV1ZSA9IHNldFRpbWVvdXQoc2hvd1BvcHVwUXVldWUsIDUwMDApO1x0XHRcdFx0XG5cdFx0XHR9XG5cdFx0fSwgMTAwMCk7XG5cdH1cblxuXHRmdW5jdGlvbiBzY3JvbGwoKXtcblx0XHRwb3MgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCk7XG5cblx0XHRpZigkKCcjbWFwJykubGVuZ3RoICYmIChwb3MgPiAkKCcjbWFwJykub2Zmc2V0KCkudG9wIC0gMTAwKSAmJiAhd2luZG93LmF1dG90cmlnZ2VyKXtcblx0XHRcdHdpbmRvdy5hdXRvdHJpZ2dlciA9IHRydWU7XG4vLy5tYXJrZXItYXV0b1xuXHRcdFx0aWYoJCgnLm1hcmtlci1hdXRvJykubGVuZ3RoKVxuXHRcdFx0e1xuXHRcdFx0XHQkKCcubWFya2VyLWF1dG8nKS5lYWNoKGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0YXV0b2xpc3QucHVzaCgkKHRoaXMpKTtcblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0Ly9zaHVmZmxlKGF1dG9saXN0KTtcblx0XHRcdFx0XG5cdFx0XHRcdHNob3dQb3B1cFF1ZXVlKCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0JCgnI2hhbWJ1cmdlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG5cdFx0JCh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XG5cdFx0JCgnLnNpdGUtbmF2LWMnKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XG5cdH0pO1xuXG5cdCQoJyNzaG9ydC1zdWJtaXQnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKXtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHR2YXIgZW1haWwgPSAkKCcjam9pbi1lbWFpbCcpLnZhbCgpO1xuXHRcdHZhciB6aXAgPSAkKCcjam9pbi16aXAnKS52YWwoKTtcblxuXHRcdGlmKGVtYWlsICE9ICcnICYmIHppcCAhPSAnJylcblx0XHR7XG5cdFx0XHR2YXIgZGF0YSA9IHthY3Rpb246ICdzaWdudXBfc2hvcnQnLCBlbWFpbDplbWFpbCwgemlwOnppcH07XG5cdFx0XHQkLmFqYXgoXG5cdFx0ICAgIHtcblx0XHQgICAgICAgIHR5cGU6IFwicG9zdFwiLFxuXHRcdCAgICAgICAgZGF0YVR5cGU6IFwianNvblwiLFxuXHRcdCAgICAgICAgdXJsOiBzZWl1X2FqYXguYWpheF91cmwsXG5cdFx0ICAgICAgICBkYXRhOiBkYXRhLFxuXHRcdCAgICAgICAgc3VjY2VzczogZnVuY3Rpb24obXNnKXtcblx0XHQgICAgICAgICAgICBpZihtc2cuc3RhdHVzID09ICdzdWNjZXNzJylcblx0XHQgICAgICAgICAgICB7XG5cdFx0ICAgICAgICAgICAgXHQkKCcjc2hvcnQtc2lnbnVwLWZvcm0nKS5zbGlkZVVwKCk7XG5cdFx0ICAgICAgICAgICAgXHQkKCcjc2hvcnQtc2lnbnVwLWVycm9ycycpLmh0bWwobXNnLm1lc3NhZ2UpLmFkZENsYXNzKCdzdWNjZXNzJykuc2xpZGVEb3duKCk7XG5cdFx0ICAgICAgICAgICAgfVxuXHRcdCAgICAgICAgICAgIGVsc2Vcblx0XHQgICAgICAgICAgICB7XG5cdFx0ICAgICAgICAgICAgXHQkKCcjc2hvcnQtc2lnbnVwLWVycm9ycycpLmh0bWwobXNnLm1lc3NhZ2UpLnJlbW92ZUNsYXNzKCdzdWNjZXNzJykuc2xpZGVEb3duKCk7XG5cdFx0ICAgICAgICAgICAgfVxuXHRcdCAgICAgICAgfVxuXHRcdCAgICB9KTtcdFxuXHRcdH1cblx0XHRlbHNlXG5cdFx0e1xuXHRcdFx0JCgnI3Nob3J0LXNpZ251cC1lcnJvcnMnKS5odG1sKCdQbGVhc2UgZW50ZXIgYSB2YWxpZCBlbWFpbCBhZGRyZXNzIGFuZCB6aXAgY29kZS4nKS5yZW1vdmVDbGFzcygnc3VjY2VzcycpLnNsaWRlRG93bigpO1xuXHRcdH1cblx0fSk7XG5cblx0JCgnLmFjY29yZGlvbiBsaSBoMycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG5cdFx0dmFyIHBhciA9ICQodGhpcykucGFyZW50KCk7XG5cblx0XHRpZigkKHRoaXMpLnBhcmVudCgpLmhhc0NsYXNzKCdhY3RpdmUnKSlcblx0XHR7XG5cdFx0XHRwYXIuZmluZCgnLmFuc3dlcicpLnNsaWRlVXAoNTAwLCBmdW5jdGlvbigpe1xuXHRcdFx0XHQkKHBhcikucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHRcdGVsc2Vcblx0XHR7XG5cdFx0XHQkKCcuYWNjb3JkaW9uIGxpLmFjdGl2ZSAuYW5zd2VyJykuc2xpZGVVcCg1MDAsIGZ1bmN0aW9uKCl7XG5cdFx0XHRcdCQodGhpcykucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0fSk7XG5cdFx0XHQkKHRoaXMpLnNpYmxpbmdzKCcuYW5zd2VyJykuc2xpZGVEb3duKDUwMCwgZnVuY3Rpb24oKXtcblx0XHRcdFx0JChwYXIpLmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0JCgnaHRtbCxib2R5JykuYW5pbWF0ZSh7c2Nyb2xsVG9wOiQocGFyKS5vZmZzZXQoKS50b3AgLSA1MH0sIDUwMCk7XG5cdFx0XHR9KTtcblx0XHR9XG5cdH0pO1xuXG5cdCQoJy5zaXRlLW5hdi1jIHVsID5saS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuID5hJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSl7XG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHR9KTtcblxuXHQkKCcucy10cmlnZ2VyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcblx0XHQkKHRoaXMpLmZhZGVPdXQoKTtcblx0XHQkKCcjc2VhcmNoJykuZmFkZUluKCk7XG5cdFx0JCgnLnMtY2xvc2UnKS5mYWRlSW4oKTtcblxuXHRcdGlmKHdpbmRvdy5tYXRjaE1lZGlhKFwiKG1pbi13aWR0aDogMTAyNHB4KVwiKS5tYXRjaGVzKXtcblx0XHRcdCQoJy5zaXRlLW5hdi1jJykuYWRkQ2xhc3MoJ21vdmluZycpO1xuXHRcdH1cblxuXHR9KTtcblxuXHQkKCcucy1jbG9zZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG5cdFx0JCgnLnMtdHJpZ2dlcicpLmZhZGVJbigpO1xuXHRcdCQoJyNzZWFyY2gnKS5mYWRlT3V0KCk7XG5cdFx0JCh0aGlzKS5mYWRlT3V0KCk7XG5cblx0XHRpZih3aW5kb3cubWF0Y2hNZWRpYShcIihtaW4td2lkdGg6IDEwMjRweClcIikubWF0Y2hlcyl7XG5cdFx0XHQkKCcuc2l0ZS1uYXYtYycpLnJlbW92ZUNsYXNzKCdtb3ZpbmcnKTtcblx0XHR9XG5cdH0pO1xuXG5cdCQoJy52b2ljZS1vdmVybGF5Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcblx0XHRzZXRQb3B1cCgkKHRoaXMpLmRhdGEoJ2knKSk7XG5cdH0pO1xuXG5cdCQoJy52b2ljZS1jbG9zZSwgLnBvcHVwLXRyaWdnZXInKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuXHRcdCQoJyN2b2ljZS1wb3B1cC1jb250ZW50cycpLmh0bWwoJycpO1xuXHRcdCQoJyN2b2ljZS1wb3B1cCcpLmZhZGVPdXQoKTtcblx0fSk7XG5cblx0JCgnLnZvaWNlLXByZXYnKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuXHRcdHZhciBjdXIgPSBwYXJzZUludCgkKCcjdm9pY2UtcG9wdXAtY29udGVudHMnKS5kYXRhKCdjdXInKSk7XG5cdFx0Y3VyIC09IDE7XG5cdFx0aWYoY3VyIDwgMClcblx0XHR7XG5cdFx0XHRjdXIgPSAkKCcudm9pY2UnKS5sZW5ndGggLSAxO1xuXHRcdH1cblxuXHRcdHNldFBvcHVwKGN1cik7XG5cdH0pO1xuXG5cdCQoJy52b2ljZS1uZXh0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcblx0XHR2YXIgY3VyID0gcGFyc2VJbnQoJCgnI3ZvaWNlLXBvcHVwLWNvbnRlbnRzJykuZGF0YSgnY3VyJykpO1xuXHRcdGN1ciArPSAxO1xuXHRcdGlmKGN1ciA+ICQoJy52b2ljZScpLmxlbmd0aCAtIDEpXG5cdFx0e1xuXHRcdFx0Y3VyID0gMDtcblx0XHR9XG5cblx0XHRzZXRQb3B1cChjdXIpO1xuXHR9KTtcblxuXHQkKCcudm0tbGVmdCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG5cdFx0dm9pY2Uuc2xpZGUoJ3ByZXYnKTtcblx0fSk7XG5cblx0JCgnLnZtLXJpZ2h0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcblx0XHR2b2ljZS5zbGlkZSgnbmV4dCcpO1xuXHR9KTtcblxuXHRmdW5jdGlvbiBzZXRQb3B1cChpKXtcblx0XHRpZih2b2ljZS52aWV3YWJsZSA8PSAxKVxuXHRcdFx0cmV0dXJuO1xuXG5cdFx0JCgnI3ZvaWNlLXBvcHVwLWNvbnRlbnRzJykuZGF0YSgnY3VyJywgaSk7XG5cdFx0JCgnI3ZvaWNlLXBvcHVwLWNvbnRlbnRzIC5wb3B1cC1pbm5lcicpLmFkZENsYXNzKCd0cmFuc2l0aW9uJyk7XG5cblx0XHR2YXIgaW1hZ2UgPSAkKCcjdm9pY2UtJyArIGkgKyAnIC52b2ljZS1vdmVybGF5JykuZGF0YSgnaW1hZ2UnKTtcblx0XHR2YXIgY29udCA9ICQoJzxkaXY+JykuYWRkQ2xhc3MoJ3BvcHVwLWlubmVyIG5ldycpO1xuXHRcdHZhciBpbSA9IGNvbnQuYXBwZW5kKCQoJzxpbWc+JykuYXR0cignc3JjJywgaW1hZ2UpKTtcblx0XHR2YXIgY29weSA9IGNvbnQuYXBwZW5kKCQoJyN2b2ljZS0nICsgaSArICcgLnZvaWNlLW92ZXJsYXknKS5odG1sKCkpO1xuXG5cdFx0JCgnI3ZvaWNlLXBvcHVwLWNvbnRlbnRzJykuYXBwZW5kKGNvbnQpO1xuXG5cdFx0aWYoJCgnI3ZvaWNlLXBvcHVwJykuaXMoJzp2aXNpYmxlJykpXG5cdFx0e1xuXHRcdFx0Y29udC5mYWRlSW4oNTAwLCBmdW5jdGlvbigpe1xuXHRcdFx0XHQkKCcucG9wdXAtaW5uZXIudHJhbnNpdGlvbicpLnJlbW92ZSgpO1xuXHRcdFx0XHRjb250LnJlbW92ZUNsYXNzKCduZXcnKTtcblx0XHRcdH0pO1xuXHRcdH1cblx0XHRlbHNlXG5cdFx0e1xuXHRcdFx0Y29udC5jc3MoJ2Rpc3BsYXknLCAnYmxvY2snKTtcblx0XHRcdCQoJyN2b2ljZS1wb3B1cCcpLmZhZGVJbigpO1x0XHRcdFxuXHRcdH1cblxuXHR9XG5cbiAgaWYoJCgnI21hcCcpLmxlbmd0aClcbiAge1xuXG4gICAgTC5tYXBib3guYWNjZXNzVG9rZW4gPSAncGsuZXlKMUlqb2lZbkpwYm10dFpXUnBZU0lzSW1FaU9pSmphbmN3ZGpacE9UY3dabWd5TkdKMFoyTTJaMnRwTjJaMEluMC5MMWk1WWI3VlZnRHRHandxU1JFOVFBJztcbiAgICB2YXIgYm91bmRzID0gW1xuICAgIFx0WzQwLjgsIC0xMjBdLCBcbiAgICBcdFs0MS41LCAtODBdXG4gICAgXTtcbiAgICB2YXIgbWFwVHdvID0gTC5tYXBib3gubWFwKCdtYXAnKS5zZXRWaWV3KFs0MC44LCAtMTAwXSwgNSk7XG4gICAgbWFwVHdvLmZpdEJvdW5kcyhib3VuZHMpO1xuXG4gICAgTC5tYXBib3guc3R5bGVMYXllcignbWFwYm94Oi8vc3R5bGVzL2JyaW5rbWVkaWEvY2oxM3NtMDJyMDAwNTJycDFhbHNmamRleicpLmFkZFRvKG1hcFR3byk7XG4gICAgdmFyIG15TGF5ZXIgPSBMLm1hcGJveC5mZWF0dXJlTGF5ZXIoKS5hZGRUbyhtYXBUd28pO1xuXG4gICAgbXlMYXllci5vbignbGF5ZXJhZGQnLCBmdW5jdGlvbihlKSB7XG5cdFx0dmFyIG1hcmtlciA9IGUubGF5ZXIsXG5cdFx0ZmVhdHVyZSA9IG1hcmtlci5mZWF0dXJlO1xuXG5cdFx0bWFya2VyLnNldEljb24oTC5kaXZJY29uKHtcbiAgICAgICAgICBjbGFzc05hbWU6IGZlYXR1cmUucHJvcGVydGllcy50eXBlLCBcbiAgICAgICAgICBpY29uU2l6ZTogbnVsbCBcbiAgICAgICAgfSkpO1xuXG5cblx0XHR2YXIgcG9wdXBDb250ZW50ID0gICc8ZGl2IGNsYXNzPVwicG9wdXAnICsgKGZlYXR1cmUucHJvcGVydGllcy5pbWFnZSA9PSAnJyA/ICcgbm8taW1hZ2UnIDogJycpICsgJ1wiPicgK1xuXHRcdFx0XHRcdFx0XHQoZmVhdHVyZS5wcm9wZXJ0aWVzLmltYWdlICE9ICcnID8gJzxkaXYgY2xhc3M9XCJwb3B1cC1pbWFnZVwiPjxpbWcgc3JjPVwiJyArIGZlYXR1cmUucHJvcGVydGllcy5pbWFnZSArICdcIiBhbHQ9XCJcIj4nIDogJycpICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGgyPicgKyBmZWF0dXJlLnByb3BlcnRpZXMudGl0bGUgKyAnPC9oMj4nICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGgzPicgKyBmZWF0dXJlLnByb3BlcnRpZXMuc3VidGl0bGUgKyAnPC9oMz4nO1xuXG4gICAgICAgIGlmKGZlYXR1cmUucHJvcGVydGllcy5kZXNjcmlwdGlvbiAhPSAnJyl7XG4gICAgICAgIFx0cG9wdXBDb250ZW50ICs9ICc8cD4nICsgZmVhdHVyZS5wcm9wZXJ0aWVzLmRlc2NyaXB0aW9uICsgJzwvcD4nO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYoZmVhdHVyZS5wcm9wZXJ0aWVzLmxpbmtfdXJsICE9ICcnKXtcbiAgICAgICAgXHRwb3B1cENvbnRlbnQgKz0gJzxwPjxhIGhyZWY9XCInICsgZmVhdHVyZS5wcm9wZXJ0aWVzLmxpbmtfdXJsICsgJ1wiIHRhcmdldD1cIl9ibGFua1wiIGNsYXNzPVwicG9wdXAtcmVhZC1tb3JlXCI+UmVhZCBNb3JlPC9hPjwvcD4nXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHBvcHVwQ29udGVudCArPSAnPC9kaXY+PHNwYW4gY2xhc3M9XCJwb3B1cC1jbG9zZVwiPlg8L3NwYW4+PC9kaXY+JztcblxuXG5cdFx0bWFya2VyLmJpbmRQb3B1cChwb3B1cENvbnRlbnQse1xuXHRcdFx0Y2xvc2VCdXR0b246IGZhbHNlLFxuXHRcdFx0bWluV2lkdGg6IDMyMFxuXHRcdH0pO1xuXG5cdFx0aWYoZmVhdHVyZS5wcm9wZXJ0aWVzLmF1dG8gPT0gJ3llcycpXG5cdFx0e1xuXHRcdFx0JChtYXJrZXIuX2ljb24pLmFkZENsYXNzKCdtYXJrZXItYXV0bycpO1xuXHRcdH1cblxuXHRcdG1hcmtlci5vbigncG9wdXBjbG9zZScsIGZ1bmN0aW9uKGUpIHtcblx0XHQgICAgJChtYXJrZXIuX2ljb24pLnJlbW92ZUNsYXNzKCdzZWxlY3RlZE1hcmtlcicpO1xuXHRcdH0pO1xuXG5cdFx0bWFya2VyLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuXHRcdCAgICAkKG1hcmtlci5faWNvbikuYWRkQ2xhc3MoJ3NlbGVjdGVkTWFya2VyJyk7XG5cdFx0ICAgIHZhciBsbCA9IHRoaXMuZ2V0TGF0TG5nKCk7XG5cdFx0ICAgIHZhciBjeiA9IG1hcFR3by5nZXRab29tKCk7XG5cdFx0ICAgIHZhciBubGw7XG5cdFx0ICAgIGlmKGN6ID09IDUpXG5cdFx0ICAgIHtcblx0XHRcdFx0bmxsID0ge1xuXHRcdFx0ICAgIFx0bGF0OiAobGwubGF0ICsgNSksXG5cdFx0XHQgICAgXHRsbmc6IGxsLmxuZ1xuXHRcdFx0ICAgIH07XG5cdFx0ICAgIH1cblx0XHQgICAgZWxzZSBpZihjeiA9PSA2KVxuXHRcdCAgICB7XG5cdFx0XHRcdG5sbCA9IHtcblx0XHRcdCAgICBcdGxhdDogKGxsLmxhdCArIDMpLFxuXHRcdFx0ICAgIFx0bG5nOiBsbC5sbmdcblx0XHRcdCAgICB9O1xuXHRcdCAgICB9XG5cdFx0ICAgIGVsc2Vcblx0XHQgICAge1xuXHRcdFx0XHRubGwgPSB7XG5cdFx0XHQgICAgXHRsYXQ6IChsbC5sYXQgKyAxKSxcblx0XHRcdCAgICBcdGxuZzogbGwubG5nXG5cdFx0XHQgICAgfTtcblx0XHQgICAgfVxuXG5cdFx0ICAgIG1hcFR3by5wYW5UbyhubGwpO1xuXHRcdCAgICAvL2NvbnNvbGUubG9nKGN6LCBubGwpO1xuXHRcdCAgICBjbGVhclRpbWVvdXQod2luZG93LmF1dG9xdWV1ZSk7XG5cdFx0fSk7XG4gICAgfSk7XG5cbiAgICBteUxheWVyLnNldEdlb0pTT04oZGF0YSk7XG4gICAgaWYgKG1hcFR3by5zY3JvbGxXaGVlbFpvb20pIHtcbiAgICAgIG1hcFR3by5zY3JvbGxXaGVlbFpvb20uZGlzYWJsZSgpO1xuICAgIH1cblxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcucG9wdXAtY2xvc2UnLCBmdW5jdGlvbigpe1xuICAgIFx0bWFwVHdvLmNsb3NlUG9wdXAoKTtcbiAgICBcdCQoJy5sZWFmbGV0LW1hcmtlci1pY29uJykucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkTWFya2VyJyk7XG4gICAgfSk7XG4gIH1cblxuICBcdHZhciB2b2ljZSA9IHtcbiAgXHRcdGN1cjowLFxuICBcdFx0Y3c6IDAsXG4gIFx0XHR2aWV3YWJsZTogMCxcbiAgXHRcdHRvdGFsOiAwLFxuICBcdFx0aW5pdDogZnVuY3Rpb24oKXtcbiAgXHRcdFx0dm9pY2UuY3VyID0gMDtcbiAgXHRcdFx0dm9pY2UuY3cgPSB3O1xuICBcdFx0XHR2b2ljZS52aWV3YWJsZSA9IDE7XG4gIFx0XHRcdHZvaWNlLnRvdGFsID0gJCgnLnZvaWNlJykubGVuZ3RoO1xuXG4gIFx0XHRcdGlmKHdpbmRvdy5tYXRjaE1lZGlhKFwiKG1pbi13aWR0aDogNzgzcHgpXCIpLm1hdGNoZXMpXG4gIFx0XHRcdHtcbiAgXHRcdFx0XHR2b2ljZS5jdyA9IHcgLyAyO1xuICBcdFx0XHRcdHZvaWNlLnZpZXdhYmxlID0gMjtcbiAgXHRcdFx0fVxuICBcdFx0XHRpZih3aW5kb3cubWF0Y2hNZWRpYShcIihtaW4td2lkdGg6IDEwMjRweClcIikubWF0Y2hlcylcbiAgXHRcdFx0e1xuICBcdFx0XHRcdHZvaWNlLmN3ID0gdyAvIDM7XG4gIFx0XHRcdFx0dm9pY2Uudmlld2FibGUgPSAzO1xuICBcdFx0XHR9XG4gIFx0XHRcdGlmKHdpbmRvdy5tYXRjaE1lZGlhKFwiKG1pbi13aWR0aDogMTQ1MHB4KVwiKS5tYXRjaGVzKVxuICBcdFx0XHR7XG5cdFx0XHRcdHZvaWNlLmN3ID0gdyAvIDQ7XG5cdFx0XHRcdHZvaWNlLnZpZXdhYmxlID0gNDtcbiAgXHRcdFx0fVxuXG4gIFx0XHRcdCQoJy52b2ljZScpLmNzcyh7d2lkdGg6dm9pY2UuY3d9KTtcblxuICBcdFx0XHRpZih2b2ljZS50b3RhbCA8PSB2b2ljZS52aWV3YWJsZSB8fCB2b2ljZS52aWV3YWJsZSA8PSAxKVxuICBcdFx0XHR7XG4gIFx0XHRcdFx0JCgnLnZtLWxlZnQsIC52bS1yaWdodCcpLmhpZGUoKTtcbiAgXHRcdFx0fVxuICBcdFx0XHRlbHNlXG4gIFx0XHRcdHtcblx0XHRcdFx0JCgnLnZtLWxlZnQsIC52bS1yaWdodCcpLnNob3coKTtcbiAgXHRcdFx0fVxuXG4gIFx0XHRcdCQoJy52b2ljZS1zbGlkZXInKS5jc3Moe3dpZHRoOiAodm9pY2UudG90YWwgKyAyKSAqIHZvaWNlLmN3LCBtYXJnaW5MZWZ0OjAgfSk7XG4gIFx0XHR9LFxuICBcdFx0c2xpZGU6IGZ1bmN0aW9uKGRpcil7XG4gIFx0XHRcdHZhciBsZWZ0ID0gdm9pY2UudG90YWwgLSB2b2ljZS5jdXI7XG5cbiAgXHRcdFx0aWYoZGlyID09ICduZXh0JylcbiAgXHRcdFx0e1xuICBcdFx0XHRcdHZvaWNlLmN1ciArPSAxO1xuXG4gIFx0XHRcdFx0aWYobGVmdCA8PSB2b2ljZS52aWV3YWJsZSlcbiAgXHRcdFx0XHR7XG4gIFx0XHRcdFx0XHRjb25zb2xlLmxvZyh2b2ljZS50b3RhbCAtIHZvaWNlLnZpZXdhYmxlLCB2b2ljZS5jdXIsICgodm9pY2UudG90YWwgLSB2b2ljZS52aWV3YWJsZSkgKiB2b2ljZS5jdykgKiAtMSk7XG4gIFx0XHRcdFx0XHQkKCcudm9pY2Utc2xpZGVyJykuYXBwZW5kKCQoJy52b2ljZScpLmZpcnN0KCkuZGV0YWNoKCkpLmNzcyh7bWFyZ2luTGVmdDogKCgodm9pY2UudG90YWwgLSB2b2ljZS52aWV3YWJsZSkgLSAxKSAqIHZvaWNlLmN3KSAqIC0xfSk7XG4gIFx0XHRcdFx0XHRjeWNsZSA9IHRydWU7XG4gIFx0XHRcdFx0XHR2b2ljZS5jdXIgLT0gMTtcbiAgXHRcdFx0XHR9XG5cbiAgXHRcdFx0XHQkKCcudm9pY2Utc2xpZGVyJykuYW5pbWF0ZSh7bWFyZ2luTGVmdDogKHZvaWNlLmN1ciAqIHZvaWNlLmN3KSAqIC0xfSwgNTAwKTtcbiAgXHRcdFx0fVxuICBcdFx0XHRlbHNlXG4gIFx0XHRcdHtcbiAgXHRcdFx0XHR2b2ljZS5jdXIgLT0gMTtcbiAgXHRcdFx0XHRpZih2b2ljZS5jdXIgPCAwKVxuICBcdFx0XHRcdHtcbiAgXHRcdFx0XHRcdCQoJy52b2ljZS1zbGlkZXInKS5wcmVwZW5kKCQoJy52b2ljZScpLmxhc3QoKS5kZXRhY2goKSkuY3NzKHttYXJnaW5MZWZ0OnZvaWNlLmN3ICogLTF9KTtcbiAgXHRcdFx0XHRcdHZvaWNlLmN1ciA9IDA7XG4gIFx0XHRcdFx0fVxuXG4gIFx0XHRcdFx0JCgnLnZvaWNlLXNsaWRlcicpLmFuaW1hdGUoe21hcmdpbkxlZnQ6IHZvaWNlLmN1ciAqIHZvaWNlLmN3ICogLTF9LCA1MDApO1xuICBcdFx0XHR9XG4gIFx0XHR9XG4gIFx0fTtcblxuXHR2YXIgc2xpZGVzaG93ID0ge1xuXHRcdGN1cjowLFxuXHRcdGRlbGF5OiA1MDAwLFxuXHRcdGF1dG9zY3JvbGw6ZmFsc2UsXG5cdFx0cXVldWU6IG51bGwsXG5cdFx0bW92aW5nOmZhbHNlLFxuXHRcdGRvdENsaWNrOiBudWxsLFxuXHRcdGluaXQ6IGZ1bmN0aW9uKCl7XG5cdFx0XHRpZigkKCcuc2xpZGUnKS5sZW5ndGggPiAxKVxuXHRcdFx0e1xuXHRcdFx0XHR2YXIgbGk7XG5cdFx0XHRcdGZvcih2YXIgbiA9IDA7IG4gPCAkKCcuc2xpZGUnKS5sZW5ndGg7IG4gKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRsaSA9ICQoJzxsaT4nKS5hZGRDbGFzcygnZG90JykuYXR0cignZGF0YS1zbGlkZScsIG4pLmF0dHIoJ2lkJywgJ2RvdC0nICsgbik7XG5cdFx0XHRcdFx0aWYobiA9PSAwKVxuXHRcdFx0XHRcdFx0bGkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXG5cdFx0XHRcdFx0bGkuYXBwZW5kVG8oJCgnLmRvdHMnKSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdGVsc2Vcblx0XHRcdHtcblx0XHRcdFx0JCgnLnNsaWRlc2hvdy1uYXYnKS5oaWRlKCk7XG5cdFx0XHR9XG5cdFx0XHRzbGlkZXNob3cuYmVnaW5TbGlkZSgpO1xuXHRcdH0sXG5cdFx0c2Nyb2xsOmZ1bmN0aW9uKGRpcil7XG5cdFx0XHRpZihzbGlkZXNob3cubW92aW5nKVxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cblx0XHRcdHNsaWRlc2hvdy5tb3ZpbmcgPSB0cnVlO1xuXHRcdFx0dmFyIGJveCwgdHdlZW4sIGJvdXQ7XG5cdFx0XHRpZihkaXIgPT0gJ25leHQnKVxuXHRcdFx0e1xuXHRcdFx0XHRib3V0ID0gJCgnI3NsaWRlLScgKyBzbGlkZXNob3cuY3VyKTtcblx0XHRcdFx0Lypcblx0XHRcdFx0dHdlZW4gPSBUd2VlbkxpdGUudG8oYm91dCwgMSwge1xuXHRcdFx0XHRcdCB4OiAkKHdpbmRvdykub3V0ZXJXaWR0aCgpICogLTEsXG5cdFx0XHRcdFx0IGVhc2U6IFBvd2VyMS5lYXNlSW5PdXQsXG5cdFx0XHRcdFx0IGRlbGF5OiAuMixcblx0XHRcdFx0XHQgb25Db21wbGV0ZTogZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRcdGJvdXQucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcdCovXG5cblx0XHRcdFx0aWYoc2xpZGVzaG93LmRvdENsaWNrICE9IG51bGwpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRzbGlkZXNob3cuY3VyID0gc2xpZGVzaG93LmRvdENsaWNrO1xuXHRcdFx0XHRcdHNsaWRlc2hvdy5kb3RDbGljayA9IG51bGw7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0c2xpZGVzaG93LmN1ciArPSAxO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0aWYoc2xpZGVzaG93LmN1ciA+ICQoJy5zbGlkZScpLmxlbmd0aCAtIDEpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRzbGlkZXNob3cuY3VyID0gMDtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8qXG5cdFx0XHRcdHR3ZWVuID0gVHdlZW5MaXRlLnRvKCQoJyNzbGlkZS0nICsgc2xpZGVzaG93LmN1ciksIDAsIHtcblx0XHRcdFx0XHQgeDogJCh3aW5kb3cpLm91dGVyV2lkdGgoKSxcblx0XHRcdFx0XHQgZGVsYXk6IDAsXG5cdFx0XHRcdFx0IG9uQ29tcGxldGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHQkKCcjc2xpZGUtJyArIHNsaWRlc2hvdy5jdXIpLmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHQqL1xuXHRcdFx0fVxuXHRcdFx0ZWxzZVxuXHRcdFx0e1xuXHRcdFx0XHRib3V0ID0gJCgnI3NsaWRlLScgKyBzbGlkZXNob3cuY3VyKTtcblx0XHRcdFx0Lypcblx0XHRcdFx0dHdlZW4gPSBUd2VlbkxpdGUudG8oYm91dCwgMSwge1xuXHRcdFx0XHRcdCB4OiAkKHdpbmRvdykub3V0ZXJXaWR0aCgpLFxuXHRcdFx0XHRcdCBlYXNlOiBQb3dlcjEuZWFzZUluT3V0LFxuXHRcdFx0XHRcdCBkZWxheTogLjIsXG5cdFx0XHRcdFx0IG9uQ29tcGxldGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHRib3V0LnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHQqL1xuXG5cdFx0XHRcdGlmKHNsaWRlc2hvdy5kb3RDbGljayAhPSBudWxsKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0c2xpZGVzaG93LmN1ciA9IHNsaWRlc2hvdy5kb3RDbGljaztcblx0XHRcdFx0XHRzbGlkZXNob3cuZG90Q2xpY2sgPSBudWxsO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHNsaWRlc2hvdy5jdXIgLT0gMTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKHNsaWRlc2hvdy5jdXIgPCAwKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0c2xpZGVzaG93LmN1ciA9ICQoJy5zbGlkZScpLmxlbmd0aCAtIDE7XG5cdFx0XHRcdH1cblx0XHRcdFx0Lypcblx0XHRcdFx0dHdlZW4gPSBUd2VlbkxpdGUudG8oJCgnI3NsaWRlLScgKyBzbGlkZXNob3cuY3VyKSwgMCwge1xuXHRcdFx0XHRcdCB4OiAkKHdpbmRvdykub3V0ZXJXaWR0aCgpICogLTEsXG5cdFx0XHRcdFx0IGRlbGF5OiAwLFxuXHRcdFx0XHRcdCBvbkNvbXBsZXRlOiBmdW5jdGlvbigpe1xuXHRcdFx0XHRcdFx0JCgnI3NsaWRlLScgKyBzbGlkZXNob3cuY3VyKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdFx0Ki9cblxuXHRcdFx0fVxuXG5cdFx0XHRib3V0LmNzcyh7ekluZGV4OjF9KTtcblx0XHRcdCQoJyNzbGlkZS0nICsgc2xpZGVzaG93LmN1cikuY3NzKHt6SW5kZXg6Miwgb3BhY2l0eTowfSkuYW5pbWF0ZSh7b3BhY2l0eToxfSwgMTAwMCwgZnVuY3Rpb24oKXtcblx0XHRcdFx0JCgnI3NsaWRlLScgKyBzbGlkZXNob3cuY3VyKS5hZGRDbGFzcygnYWN0aXZlJykuY3NzKHt6SW5kZXg6MX0pO1xuXHRcdFx0XHRib3V0LnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5jc3Moe29wYWNpdHk6MCwgekluZGV4OjB9KTtcblxuXHRcdFx0XHRpZihzbGlkZXNob3cuYXV0b3Njcm9sbClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHNsaWRlc2hvdy5xdWV1ZSA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRcdHNsaWRlc2hvdy5zY3JvbGwoJ25leHQnKTtcblx0XHRcdFx0XHR9LCBzbGlkZXNob3cuZGVsYXkpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0c2xpZGVzaG93Lm1vdmluZyA9IGZhbHNlO1xuXG5cdFx0XHR9KTtcblxuXHRcdFx0Ym94ID0gJCgnI3NsaWRlLScgKyBzbGlkZXNob3cuY3VyKTtcblx0XHRcdCQoJy5kb3QuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0JCgnI2RvdC0nICsgc2xpZGVzaG93LmN1cikuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0Ly8gQ3JlYXRlIGEgdHdlZW5cblx0XHRcdC8qXG5cdFx0XHR0d2VlbiA9IFR3ZWVuTGl0ZS50byhib3gsIDEsIHtcblx0XHRcdCB4OiAwLFxuXHRcdFx0IGVhc2U6IFBvd2VyMS5lYXNlSW5PdXQsXG5cdFx0XHQgZGVsYXk6IC4yLFxuXHRcdFx0IG9uQ29tcGxldGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdGlmKHNsaWRlc2hvdy5hdXRvc2Nyb2xsKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0c2xpZGVzaG93LnF1ZXVlID0gc2V0VGltZW91dChmdW5jdGlvbigpe1xuXHRcdFx0XHRcdFx0c2xpZGVzaG93LnNjcm9sbCgnbmV4dCcpO1xuXHRcdFx0XHRcdH0sIHNsaWRlc2hvdy5kZWxheSk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRzbGlkZXNob3cubW92aW5nID0gZmFsc2U7XG5cdFx0XHR9XG5cdFx0XHR9KTsqL1xuXHRcdH0sXG5cdFx0YmVnaW5TbGlkZTogZnVuY3Rpb24oKXtcblx0XHRcdHNsaWRlc2hvdy5hdXRvc2Nyb2xsID0gdHJ1ZTtcblx0XHRcdHNsaWRlc2hvdy5xdWV1ZSA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblx0XHRcdFx0c2xpZGVzaG93LnNjcm9sbCgnbmV4dCcpO1xuXHRcdFx0fSwgc2xpZGVzaG93LmRlbGF5KTtcblx0XHR9XG5cdH07XG5cblx0JCgnLnNsaWRlc2hvdy1uYXYnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKXtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHRzbGlkZXNob3cuYXV0b3Njcm9sbCA9IGZhbHNlO1xuXHRcdGNsZWFyVGltZW91dChzbGlkZXNob3cucXVldWUpO1xuXHRcdHNsaWRlc2hvdy5zY3JvbGwoJCh0aGlzKS5hdHRyKCdpZCcpKTtcblx0XHQkKCcudmlkZW8tb3ZlcmxheScpLnJlbW92ZSgpO1xuXHRcdCQoJy5vdmVybGF5LXRleHQnKS5mYWRlSW4oKTtcblx0fSk7XG5cblx0JChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5kb3QnLCBmdW5jdGlvbihlKXtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHRzbGlkZXNob3cuZG90Q2xpY2sgPSAkKHRoaXMpLmRhdGEoJ3NsaWRlJyk7XG5cblx0XHRpZihzbGlkZXNob3cuZG90Q2xpY2sgPT0gc2xpZGVzaG93LmN1cilcblx0XHR7XG5cdFx0XHRzbGlkZXNob3cuZG90Q2xpY2sgPSBudWxsO1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXG5cdFx0c2xpZGVzaG93LmF1dG9zY3JvbGwgPSBmYWxzZTtcblx0XHRjbGVhclRpbWVvdXQoc2xpZGVzaG93LnF1ZXVlKTtcblx0XHQkKCcudmlkZW8tb3ZlcmxheScpLnJlbW92ZSgpO1xuXHRcdCQoJy5vdmVybGF5LXRleHQnKS5mYWRlSW4oKTtcblx0XHRpZihzbGlkZXNob3cuZG90Q2xpY2sgPCBzbGlkZXNob3cuY3VyKVxuXHRcdFx0c2xpZGVzaG93LnNjcm9sbCgncHJldicpO1xuXHRcdGVsc2Vcblx0XHRcdHNsaWRlc2hvdy5zY3JvbGwoJ25leHQnKTtcblx0fSk7XG5cblx0JCgnLnZpZGVvLXBsYXknKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuXHRcdGNsZWFySW50ZXJ2YWwoc2xpZGVzaG93LnF1ZXVlKTtcblx0XHR2YXIgdmlkID0gJCh0aGlzKS5kYXRhKCd1cmwnKTtcblx0XHQkKCcub3ZlcmxheS10ZXh0JykuZmFkZU91dCgpO1xuXHRcdCQodGhpcykucGFyZW50KCkucGFyZW50KCkuYXBwZW5kKCQoJzxkaXY+JykuYWRkQ2xhc3MoJ3ZpZGVvLW92ZXJsYXknKS5odG1sKCc8ZGl2IGNsYXNzPVwidmlkZW9XcmFwcGVyXCI+PGlmcmFtZSBzcmM9XCInICsgdmlkICsgJ1wiIGZyYW1lYm9yZGVyPVwiMFwiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT48L2Rpdj4nKSk7XG5cdFx0JCgnLnZpZGVvLWNsb3NlJykuc2xpZGVEb3duKCk7XG5cdH0pO1xuXG5cdCQoJy52aWRlby1jbG9zZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG5cdFx0JCgnLnZpZGVvLW92ZXJsYXknKS5yZW1vdmUoKTtcblx0XHQkKCcub3ZlcmxheS10ZXh0JykuZmFkZUluKCk7XG5cdFx0JCgnLnZpZGVvLWNsb3NlJykuc2xpZGVVcCgpO1xuXHRcdHNsaWRlc2hvdy5pbml0KCk7XG5cdH0pO1xufSk7Il19
