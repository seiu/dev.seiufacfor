<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package seiu
 *
 * AS YOU ADD JAVASCRIPT files here, please consider adding them
 * to to the uglification process in Gruntfile.js.
 */
?>

    	</div>

    	<footer class="site-footer">
        <div class="container">
          <div class="footer-cta">
            <h3>Stay in touch and join our email list today!</h3>
            <a class="blue-button" href="/mailing-list/">Join Us</a>
          </div>
          <div class="footer-social">
            <a href="https://www.facebook.com/seiufacultyforward" class="facebook-link" target="_blank"></a>
            <a href="https://twitter.com/FacultyForward" class="twitter-link" target="_blank"></a>
            <p>&copy; Copyright Faculty Forward <?php echo date('Y') ?> | <a href="http://www.seiu.org/privacy/">Privacy</a></p>
          </div>
        </div>
    	</footer>
    </div>

    <?php wp_footer(); ?>

  </body>
</html>