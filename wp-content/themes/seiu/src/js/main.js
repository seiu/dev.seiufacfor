jQuery(document).ready(function($){
	var w = 0;
	var pos = 0;
	window.sliderQueue = null;

	$(window).on('scroll', scroll);
	$(window).on('resize', resize);
	setTimeout(function(){
		resize();
		scroll();

		if($('.slideshow-container').length){
			slideshow.init();
		}

		if($('.voice-slider').length){
			voice.init();
		}
	}, 500);

	function resize(){
		w = $(window).width();

		if($('.match').length){
			var max = 0;
			var left = false;
			$('.match').css('height', 'auto');
			if(window.matchMedia("(min-width: 1024px)").matches){
					$('.match').each(function(){
					if($(this).outerHeight() > max){
						if($(this).hasClass('left-side'))
							left = true;

						max = $(this).outerHeight();
					}
				});
				
				$('.match').css('height', max + (left ? 0 : 400));
			}
		}

		if($('.voice-slider').length){
			voice.init();
		}

		if($('.form-left').length)
		{
			$('.form-left, .form-right').css({minHeight:0});
			if(window.matchMedia("(min-width: 1024px)").matches){
				var fl = $('.form-left').outerHeight();
				var fr = $('.form-right').outerHeight();
				if(fl < fr)
				{
					$('.form-left, .form-right').css({minHeight:fr});
				}
				else
				{
					$('.form-left, .form-right').css({minHeight:fl});
				}
			}
		}
	}

	var autolist = [];
	window.autotrigger = false;
	window.autoqueue = null;

	function shuffle(a) {
	    var j, x, i;
	    for (i = a.length; i; i--) {
	        j = Math.floor(Math.random() * i);
	        x = a[i - 1];
	        a[i - 1] = a[j];
	        a[j] = x;
	    }
	}

	function showPopupQueue(){
		var item = autolist.pop();
		window.autoqueue = setTimeout(function(){
			item.click();

			if(autolist.length > 0)
			{
				window.autoqueue = setTimeout(showPopupQueue, 5000);				
			}
		}, 1000);
	}

	function scroll(){
		pos = $(window).scrollTop();

		if($('#map').length && (pos > $('#map').offset().top - 100) && !window.autotrigger){
			window.autotrigger = true;
//.marker-auto
			if($('.marker-auto').length)
			{
				$('.marker-auto').each(function(){
					autolist.push($(this));
				});

				//shuffle(autolist);
				
				showPopupQueue();
			}
		}
	}

	$('#hamburger').on('click', function(){
		$(this).toggleClass('active');
		$('.site-nav-c').toggleClass('active');
	});

	$('#short-submit').on('click', function(e){
		e.preventDefault();

		var email = $('#join-email').val();
		var zip = $('#join-zip').val();

		if(email != '' && zip != '')
		{
			var data = {action: 'signup_short', email:email, zip:zip};
			$.ajax(
		    {
		        type: "post",
		        dataType: "json",
		        url: seiu_ajax.ajax_url,
		        data: data,
		        success: function(msg){
		            if(msg.status == 'success')
		            {
		            	$('#short-signup-form').slideUp();
		            	$('#short-signup-errors').html(msg.message).addClass('success').slideDown();
		            }
		            else
		            {
		            	$('#short-signup-errors').html(msg.message).removeClass('success').slideDown();
		            }
		        }
		    });	
		}
		else
		{
			$('#short-signup-errors').html('Please enter a valid email address and zip code.').removeClass('success').slideDown();
		}
	});

	$('.accordion li h3').on('click', function(){
		var par = $(this).parent();

		if($(this).parent().hasClass('active'))
		{
			par.find('.answer').slideUp(500, function(){
				$(par).removeClass('active');
			});
		}
		else
		{
			$('.accordion li.active .answer').slideUp(500, function(){
				$(this).parent().removeClass('active');
			});
			$(this).siblings('.answer').slideDown(500, function(){
				$(par).addClass('active');
				$('html,body').animate({scrollTop:$(par).offset().top - 50}, 500);
			});
		}
	});

	$('.site-nav-c ul >li.menu-item-has-children >a').on('click', function(e){
		e.preventDefault();
	});

	$('.s-trigger').on('click', function(){
		$(this).fadeOut();
		$('#search').fadeIn();
		$('.s-close').fadeIn();

		if(window.matchMedia("(min-width: 1024px)").matches){
			$('.site-nav-c').addClass('moving');
		}

	});

	$('.s-close').on('click', function(){
		$('.s-trigger').fadeIn();
		$('#search').fadeOut();
		$(this).fadeOut();

		if(window.matchMedia("(min-width: 1024px)").matches){
			$('.site-nav-c').removeClass('moving');
		}
	});

	$('.voice-overlay').on('click', function(){
		setPopup($(this).data('i'));
	});

	$('.voice-close, .popup-trigger').on('click', function(){
		$('#voice-popup-contents').html('');
		$('#voice-popup').fadeOut();
	});

	$('.voice-prev').on('click', function(){
		var cur = parseInt($('#voice-popup-contents').data('cur'));
		cur -= 1;
		if(cur < 0)
		{
			cur = $('.voice').length - 1;
		}

		setPopup(cur);
	});

	$('.voice-next').on('click', function(){
		var cur = parseInt($('#voice-popup-contents').data('cur'));
		cur += 1;
		if(cur > $('.voice').length - 1)
		{
			cur = 0;
		}

		setPopup(cur);
	});

	$('.vm-left').on('click', function(){
		voice.slide('prev');
	});

	$('.vm-right').on('click', function(){
		voice.slide('next');
	});

	function setPopup(i){
		if(voice.viewable <= 1)
			return;

		$('#voice-popup-contents').data('cur', i);
		$('#voice-popup-contents .popup-inner').addClass('transition');

		var image = $('#voice-' + i + ' .voice-overlay').data('image');
		var cont = $('<div>').addClass('popup-inner new');
		var im = cont.append($('<img>').attr('src', image));
		var copy = cont.append($('#voice-' + i + ' .voice-overlay').html());

		$('#voice-popup-contents').append(cont);

		if($('#voice-popup').is(':visible'))
		{
			cont.fadeIn(500, function(){
				$('.popup-inner.transition').remove();
				cont.removeClass('new');
			});
		}
		else
		{
			cont.css('display', 'block');
			$('#voice-popup').fadeIn();			
		}

	}

  if($('#map').length)
  {

    L.mapbox.accessToken = 'pk.eyJ1IjoiYnJpbmttZWRpYSIsImEiOiJjancwdjZpOTcwZmgyNGJ0Z2M2Z2tpN2Z0In0.L1i5Yb7VVgDtGjwqSRE9QA';
    var bounds = [
    	[40.8, -120], 
    	[41.5, -80]
    ];
    var mapTwo = L.mapbox.map('map').setView([40.8, -100], 5);
    mapTwo.fitBounds(bounds);

    L.mapbox.styleLayer('mapbox://styles/brinkmedia/cj13sm02r00052rp1alsfjdez').addTo(mapTwo);
    var myLayer = L.mapbox.featureLayer().addTo(mapTwo);

    myLayer.on('layeradd', function(e) {
		var marker = e.layer,
		feature = marker.feature;

		marker.setIcon(L.divIcon({
          className: feature.properties.type, 
          iconSize: null 
        }));


		var popupContent =  '<div class="popup' + (feature.properties.image == '' ? ' no-image' : '') + '">' +
							(feature.properties.image != '' ? '<div class="popup-image"><img src="' + feature.properties.image + '" alt="">' : '') +
                            '<h2>' + feature.properties.title + '</h2>' +
                            '<h3>' + feature.properties.subtitle + '</h3>';

        if(feature.properties.description != ''){
        	popupContent += '<p>' + feature.properties.description + '</p>';
        }

        if(feature.properties.link_url != ''){
        	popupContent += '<p><a href="' + feature.properties.link_url + '" target="_blank" class="popup-read-more">Read More</a></p>'
        }
        
        popupContent += '</div><span class="popup-close">X</span></div>';


		marker.bindPopup(popupContent,{
			closeButton: false,
			minWidth: 320
		});

		if(feature.properties.auto == 'yes')
		{
			$(marker._icon).addClass('marker-auto');
		}

		marker.on('popupclose', function(e) {
		    $(marker._icon).removeClass('selectedMarker');
		});

		marker.on('click', function() {
		    $(marker._icon).addClass('selectedMarker');
		    var ll = this.getLatLng();
		    var cz = mapTwo.getZoom();
		    var nll;
		    if(cz == 5)
		    {
				nll = {
			    	lat: (ll.lat + 5),
			    	lng: ll.lng
			    };
		    }
		    else if(cz == 6)
		    {
				nll = {
			    	lat: (ll.lat + 3),
			    	lng: ll.lng
			    };
		    }
		    else
		    {
				nll = {
			    	lat: (ll.lat + 1),
			    	lng: ll.lng
			    };
		    }

		    mapTwo.panTo(nll);
		    //console.log(cz, nll);
		    clearTimeout(window.autoqueue);
		});
    });

    myLayer.setGeoJSON(data);
    if (mapTwo.scrollWheelZoom) {
      mapTwo.scrollWheelZoom.disable();
    }

    $(document).on('click', '.popup-close', function(){
    	mapTwo.closePopup();
    	$('.leaflet-marker-icon').removeClass('selectedMarker');
    });
  }

  	var voice = {
  		cur:0,
  		cw: 0,
  		viewable: 0,
  		total: 0,
  		init: function(){
  			voice.cur = 0;
  			voice.cw = w;
  			voice.viewable = 1;
  			voice.total = $('.voice').length;

  			if(window.matchMedia("(min-width: 783px)").matches)
  			{
  				voice.cw = w / 2;
  				voice.viewable = 2;
  			}
  			if(window.matchMedia("(min-width: 1024px)").matches)
  			{
  				voice.cw = w / 3;
  				voice.viewable = 3;
  			}
  			if(window.matchMedia("(min-width: 1450px)").matches)
  			{
				voice.cw = w / 4;
				voice.viewable = 4;
  			}

  			$('.voice').css({width:voice.cw});

  			if(voice.total <= voice.viewable || voice.viewable <= 1)
  			{
  				$('.vm-left, .vm-right').hide();
  			}
  			else
  			{
				$('.vm-left, .vm-right').show();
  			}

  			$('.voice-slider').css({width: (voice.total + 2) * voice.cw, marginLeft:0 });
  		},
  		slide: function(dir){
  			var left = voice.total - voice.cur;

  			if(dir == 'next')
  			{
  				voice.cur += 1;

  				if(left <= voice.viewable)
  				{
  					console.log(voice.total - voice.viewable, voice.cur, ((voice.total - voice.viewable) * voice.cw) * -1);
  					$('.voice-slider').append($('.voice').first().detach()).css({marginLeft: (((voice.total - voice.viewable) - 1) * voice.cw) * -1});
  					cycle = true;
  					voice.cur -= 1;
  				}

  				$('.voice-slider').animate({marginLeft: (voice.cur * voice.cw) * -1}, 500);
  			}
  			else
  			{
  				voice.cur -= 1;
  				if(voice.cur < 0)
  				{
  					$('.voice-slider').prepend($('.voice').last().detach()).css({marginLeft:voice.cw * -1});
  					voice.cur = 0;
  				}

  				$('.voice-slider').animate({marginLeft: voice.cur * voice.cw * -1}, 500);
  			}
  		}
  	};

	var slideshow = {
		cur:0,
		delay: 5000,
		autoscroll:false,
		queue: null,
		moving:false,
		dotClick: null,
		init: function(){
			if($('.slide').length > 1)
			{
				var li;
				for(var n = 0; n < $('.slide').length; n ++)
				{
					li = $('<li>').addClass('dot').attr('data-slide', n).attr('id', 'dot-' + n);
					if(n == 0)
						li.addClass('active');

					li.appendTo($('.dots'));
				}
			}
			else
			{
				$('.slideshow-nav').hide();
			}
			slideshow.beginSlide();
		},
		scroll:function(dir){
			if(slideshow.moving)
				return false;

			slideshow.moving = true;
			var box, tween, bout;
			if(dir == 'next')
			{
				bout = $('#slide-' + slideshow.cur);
				/*
				tween = TweenLite.to(bout, 1, {
					 x: $(window).outerWidth() * -1,
					 ease: Power1.easeInOut,
					 delay: .2,
					 onComplete: function(){
						bout.removeClass('active');
					}
				});
				*/

				if(slideshow.dotClick != null)
				{
					slideshow.cur = slideshow.dotClick;
					slideshow.dotClick = null;
				}
				else
				{
					slideshow.cur += 1;
				}

				if(slideshow.cur > $('.slide').length - 1)
				{
					slideshow.cur = 0;
				}

				/*
				tween = TweenLite.to($('#slide-' + slideshow.cur), 0, {
					 x: $(window).outerWidth(),
					 delay: 0,
					 onComplete: function(){
						$('#slide-' + slideshow.cur).addClass('active');
					}
				});
				*/
			}
			else
			{
				bout = $('#slide-' + slideshow.cur);
				/*
				tween = TweenLite.to(bout, 1, {
					 x: $(window).outerWidth(),
					 ease: Power1.easeInOut,
					 delay: .2,
					 onComplete: function(){
						bout.removeClass('active');
					}
				});
				*/

				if(slideshow.dotClick != null)
				{
					slideshow.cur = slideshow.dotClick;
					slideshow.dotClick = null;
				}
				else
				{
					slideshow.cur -= 1;
				}

				if(slideshow.cur < 0)
				{
					slideshow.cur = $('.slide').length - 1;
				}
				/*
				tween = TweenLite.to($('#slide-' + slideshow.cur), 0, {
					 x: $(window).outerWidth() * -1,
					 delay: 0,
					 onComplete: function(){
						$('#slide-' + slideshow.cur).addClass('active');
					}
				});
				*/

			}

			bout.css({zIndex:1});
			$('#slide-' + slideshow.cur).css({zIndex:2, opacity:0}).animate({opacity:1}, 1000, function(){
				$('#slide-' + slideshow.cur).addClass('active').css({zIndex:1});
				bout.removeClass('active').css({opacity:0, zIndex:0});

				if(slideshow.autoscroll)
				{
					slideshow.queue = setTimeout(function(){
						slideshow.scroll('next');
					}, slideshow.delay);
				}

				slideshow.moving = false;

			});

			box = $('#slide-' + slideshow.cur);
			$('.dot.active').removeClass('active');
			$('#dot-' + slideshow.cur).addClass('active');
			// Create a tween
			/*
			tween = TweenLite.to(box, 1, {
			 x: 0,
			 ease: Power1.easeInOut,
			 delay: .2,
			 onComplete: function(){
				if(slideshow.autoscroll)
				{
					slideshow.queue = setTimeout(function(){
						slideshow.scroll('next');
					}, slideshow.delay);
				}

				slideshow.moving = false;
			}
			});*/
		},
		beginSlide: function(){
			slideshow.autoscroll = true;
			slideshow.queue = setTimeout(function(){
				slideshow.scroll('next');
			}, slideshow.delay);
		}
	};

	$('.slideshow-nav').on('click', function(e){
		e.preventDefault();

		slideshow.autoscroll = false;
		clearTimeout(slideshow.queue);
		slideshow.scroll($(this).attr('id'));
		$('.video-overlay').remove();
		$('.overlay-text').fadeIn();
	});

	$(document).on('click', '.dot', function(e){
		e.preventDefault();

		slideshow.dotClick = $(this).data('slide');

		if(slideshow.dotClick == slideshow.cur)
		{
			slideshow.dotClick = null;
			return;
		}


		slideshow.autoscroll = false;
		clearTimeout(slideshow.queue);
		$('.video-overlay').remove();
		$('.overlay-text').fadeIn();
		if(slideshow.dotClick < slideshow.cur)
			slideshow.scroll('prev');
		else
			slideshow.scroll('next');
	});

	$('.video-play').on('click', function(){
		clearInterval(slideshow.queue);
		var vid = $(this).data('url');
		$('.overlay-text').fadeOut();
		$(this).parent().parent().append($('<div>').addClass('video-overlay').html('<div class="videoWrapper"><iframe src="' + vid + '" frameborder="0" allowfullscreen></iframe></div>'));
		$('.video-close').slideDown();
	});

	$('.video-close').on('click', function(){
		$('.video-overlay').remove();
		$('.overlay-text').fadeIn();
		$('.video-close').slideUp();
		slideshow.init();
	});
});