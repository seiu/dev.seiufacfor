<?php
/**
 * seiu functions and definitions
 *
 * @package seiu
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */

if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'seiu_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function seiu_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/extras.php' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'seiu' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
}
endif; // seiu_setup
add_action( 'after_setup_theme', 'seiu_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function seiu_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'seiu' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'seiu_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function seiu_scripts() {

	function the_scripts() {
		global $wp_scripts;
		// load third-party libs
		wp_enqueue_script('jquery');
		//wp_register_script( 'libs', get_template_directory_uri() . '/public_assets/js/libs.js', array(), false, true);
		// load compiled app
		wp_register_script( 'tween', get_template_directory_uri() . '/public_assets/js/TweenLite.min.js', array(), false, true);
	 	wp_enqueue_script('tween');
		wp_register_script( 'gsap', get_template_directory_uri() . '/public_assets/js/jquery.gsap.min.js', array(), false, true);
	 	wp_enqueue_script('gsap');
		wp_register_script( 'cssp', get_template_directory_uri() . '/public_assets/js/CSSPlugin.min.js', array(), false, true);
	 	wp_enqueue_script('cssp');

		wp_register_script( 'app', get_template_directory_uri() . '/public_assets/js/app.js', array(), false, true);
	 	wp_enqueue_script('app');

	 	wp_localize_script( 'app', 'seiu_ajax',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	 	// load css
	 	wp_register_style( 'css', get_template_directory_uri() . '/public_assets/css/main.css', array(), false, 'all' );
	 	wp_enqueue_style('css');
	}

	if (!is_admin()) 
		add_action("wp_enqueue_scripts", "the_scripts", 11);
	
}
add_action( 'wp_enqueue_scripts', 'seiu_scripts' );

/* for adding custom post types
add_action( 'init', 'seiu_create_post_type' );
function seiu_create_post_type() {

	register_post_type( 'events',
		array(
			'labels' => array(
				'name' => __( 'Events' ),
				'singular_name' => __( 'Event' )
			),
			'public' => true,
			'has_archive' => false,
			'taxonomies' => array('post_tag', 'category'),
			//'rewrite' => array('slug' => 'exhibition'),
			'supports' => array(
				'title',
				'editor',
				'thumbnail'
			)
		)
	);
}
*/

/* for adding paginated pages on custom index types
add_action( 'init', 'seiu_add_rewrite' );
function seiu_add_rewrite()
{
	//flush_rewrite_rules();
    add_rewrite_rule(
        '^example/page/([0-9]+)/?$',
        'index.php?page_id=2&paged=$matches[1]',
        'top'
    );
}
*/

function timeline_shortcode($atts) {
	$html = '<div class="timeline">';
	$html .= '<h2>Our Recent Victories</h2>';

	include('simple_html_dom.php');
	$gp = file_get_html('http://victories.seiufacultyforward.org/');

	$i = 0;
	foreach($gp->find('.cd-timeline-content') as $element) 
	{
		if($i == 5)
			continue;

		$title = $element->find('h2', 0)->plaintext;
		$img = $element->find('img', 0)->src;
		$c = $element->find('p');
		
		$html .= '<div class="timeline-item">';
		if($img)
			$html .= '<img src="' . $img .'" alt="' . str_replace('"', "'", $title) . '">';

		$html .= '<h3><a class="timeline-read-more" href="' . $element->find('.cd-read-more', 0)->href . '" target="_blank">' . $title . '</a></h3>';
		$html .= '<span class="date">' . $element->find('.cd-date', 0)->plaintext . '</span>';

		if(!isset($atts['titleonly']))
		{
			foreach($c AS $p){
				if($p->plaintext != ''){
					$html .= '<p>' . $p->plaintext . '</p>';
				}
			}			
		}

		$html .= '<a class="timeline-read-more" href="' . $element->find('.cd-read-more', 0)->href . '" target="_blank">Read More</a>';
		$html .= '</div>';
		
		$i ++;
	}
	
	$html .= '<a class="timeline-all" href="http://victories.seiufacultyforward.org/" target="_blank">See all our victories</a></div>';

	return $html;
}
add_shortcode('timeline', 'timeline_shortcode');


function seiu_truncate($text, $chars = 25, $ellipses = false) {
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    if($ellipses)
    {
    	$text .= '...';
    }
    return $text;
}


if(isset($_GET['flush']))
{
	flush_rewrite_rules();
}


function seiu_parse_blog()
{
	$segs = explode("/", $_SERVER['REQUEST_URI']);
	if($segs[1] == 'category')
	{
		$type = '/'.$segs[1].'/'.$segs[2].'/';
		$cat = $segs[2];

		$page = isset($segs[4]) ? $segs[4] : 1;	
	}
	else if($segs[1] == 'blog')
	{
		$type = '/'.$segs[1].'/';
		$cat = '';

		$page = isset($segs[3]) ? $segs[3] : 1;	
	}
	else
	{
		$type = '/';
		$cat = '';
		$page = isset($segs[2]) ? $segs[2] : 1;		
	}



	return array('page' => urldecode($page), 'type' => $type, 'category' => $cat);
}

function seiu_paginate($page, $per, $type = '/', $category = '')
{

	$args = array(
        'posts_per_page' => -1,
        'post_type'      => 'post',
        'post_status' => 'publish',
		'order' => 'DESC'
    );

    if($category != '')
    {
		$args['category_name'] = $category;
    }

	$items = new WP_Query($args);
	$total = count($items->posts);
	$pages = ceil($total / $per);

	if($pages < 2)
		return '';

	$html = '<ul class="pagination">';

	if($page > 10)
		$html .= '<li class="first"><a href="'.$type.'">&nbsp;</a></li>';

	if($page > 1)
		$html .= '<li class="previous"><a href="' . $type . ($page - 1 > 1 ? 'page/' . ($page - 1) : '').'">&nbsp;</a></li>';

	if($pages <= 10 || $page <= 10)
	{
		$max = ($pages <= 10 ? $pages : 10);
		for($i = 1; $i <= $max; $i ++)
		{
			if($i == $page)
				$html .= '<li class="current">'.$i.'</li>';
			else
				$html .= '<li><a href="'. ($type != '' ? $type : '') . ($i > 1 ? 'page/'.$i : '').'">'.$i.($pages > 10 && $i == 10 ? '...' : '').'</a></li>';
		}		
	}
	else
	{
		$low = $page - 5;
		$high = $page + 5;
		if($high > $pages)
			$high = $pages;

		for($i = $low; $i <= $high; $i ++)
		{
			if($i == $page)
				$html .= '<li class="current">'.$i.'</li>';
			else
				$html .= '<li><a href="'. ($type != '' ? $type : '') . ($i > 1 ? 'page/'.$i : '').'">'.($i == $low ? '...' : '').$i.($pages > $high && $i == $high ? '...' : '').'</a></li>';
		}	
	}


	if($page < $pages && $pages > 1)
		$html .= '<li class="next"><a href="'.$type.'page/'.($page + 1).'">&nbsp;</a></li>';

	if($pages > 10 && $page != $pages)
		$html .= '<li class="last"><a href="'.$type.'page/'.$pages.'">&nbsp;</a></li>';

	$html .= '</ul>';

	return $html;
}

add_filter("gform_address_types", "us_address", 10, 2);
function us_address($address_types, $form_id){
	$address_types["us"] = array(
	"label" => "United States",
	"country" => "USAB",
	"zip_label" => "Zip Code",
	"state_label" => "State",
	"states" => array(
	"" => "",
	"AL" => "Alabama",
	"AK" => "Alaska",
	"AZ" => "Arizona",
	"AR" => "Arkansas",
	"CA" => "California",
	"CO" => "Colorado",
	"CT" => "Connecticut",
	"DE" => "Delaware",
	"DC" => "District of Columbia",
	"FL" => "Florida",
	"GA" => "Georgia",
	"GU" => "Guam",
	"HI" => "Hawaii",
	"ID" => "Idaho",
	"IL" => "Illinois",
	"IN" => "Indiana",
	"IA" => "Iowa",
	"KS" => "Kansas",
	"KY" => "Kentucky",
	"LA" => "Louisiana",
	"ME" => "Maine",
	"MD" => "Maryland",
	"MA" => "Massachusetts",
	"MI" => "Michigan",
	"MN" => "Minnesota",
	"MS" => "Mississippi",
	"MO" => "Missouri",
	"MT" => "Montana",
	"NE" => "Nebraska",
	"NV" => "Nevada",
	"NH" => "New Hampshire",
	"NJ" => "New Jersey",
	"NM" => "New Mexico",
	"NY" => "New York",
	"NC" => "North Carolina",
	"ND" => "North Dakota",
	"OH" => "Ohio",
	"OK" => "Oklahoma",
	"OR" => "Oregon",
	"PA" => "Pennsylvania",
	"PR" => "Puerto Rico",
	"RI" => "Rhode Island",
	"SC" => "South Carolina",
	"SD" => "South Dakota",
	"TN" => "Tennessee",
	"TX" => "Texas",
	"UT" => "Utah",
	"VT" => "Vermont",
	"VA" => "Virginia",
	"WA" => "Washington",
	"WV" => "West Virginia",
	"WI" => "Wisconsin",
	"WY" => "Wyoming"
	));
	return $address_types;
}

add_action("wp_ajax_signup_short", "signup_short");
add_action("wp_ajax_nopriv_signup_short", "signup_short");

function signup_short()
{
	$res = array('status' => 'success', 'message' => 'Thank you for signing up!');

	$email = $_POST['email'];
	$zip = $_POST['zip'];

	if($email == '' || !filter_var($email, FILTER_VALIDATE_EMAIL) || $zip == '')
	{
		$res = array('status' => 'fail', 'message' => 'Please enter a valid email address and zip code.');
	}

	$action = "https://action.seiu.org/page/sapi/faculty-forward-email-signup";
	$fields = 'email='.$email.'&zip='.$zip.'&custom-177339[0]=1045677';
	$response = bsd_curl($action, $fields);
	if(!$response)
	{
		$res = array('status' => 'fail', 'message' => 'Something went wrong with the form. Please try again.');
	}
	else
	{
		ga_send_event('Short Form Submitted', 'click', '');
	}

	echo json_encode($res);
	exit;
}

add_action( 'gform_after_submission', 'send_bsd', 10, 2 );


function send_bsd($entry, $form)
{
	$action = false;
	$ga_event = '';

	if($form['id'] == 1)
	{
		// join us (mailing list) form - long version
		$ga_event = 'Mailing List (long form)';
		$action = "https://action.seiu.org/page/sapi/faculty-forward-email-signup";

		$su = rgar( $entry, '11.1' );
		if($su == '')
			$su = false;
		else
			$su = '1045677';

		$firstname = rgar( $entry, '3.3' );
		$lastname = rgar( $entry, '3.6' );
		$email = rgar( $entry, '2' );
		$addr1 = rgar( $entry, '10.1' );
		$addr2 = rgar( $entry, '10.2' );
		$city = rgar( $entry, '10.3' );
		$state = rgar( $entry, '10.4' );
		$zip = rgar( $entry, '10.5' );
		$phone = rgar( $entry, '4' );
		$occupation = rgar( $entry, '5' );
		$employer = rgar( $entry, '6' );
		$department = rgar( $entry, '7' );
		$signup = $su;


		$fields = 'firstname='.$firstname .'&lastname='.$lastname.'&email='.$email.'&addr1='.$addr1.'&addr2='.$addr2.'&city='.$city.'&state_cd='.$state.'&zip='.$zip.'&phone='.$phone.'&occupation='.$occupation.'&employer='.$employer.'&custom-177006='.$department.'&custom-177339[0]='.$signup;

	}
	else if($form['id'] == 3 || $form['id'] == 4)
	{
		// 3 is grad connect form
		// 4 is faculty connect form
		// both are identical but they need to send a hidden field that I can't set in grav forms

		$action = "https://action.seiu.org/page/sapi/gradstudentsforward";

		$su = rgar( $entry, '13.1' );
		if($su == '')
			$su = false;
		else
			$su = '1035486';

		if($form['id'] == 3)
		{
			$formtype = 'grad student';
			$ga_event = 'Grad Student Form';		
		}
		else
		{
			$formtype = 'faculty';	
			$ga_event = 'Faculty Form';		
		}


		$firstname = rgar( $entry, '3.3' );
		$lastname = rgar( $entry, '3.6' );
		$email = rgar( $entry, '2' );
		$addr1 = rgar( $entry, '10.1' );
		$addr2 = rgar( $entry, '10.2' );
		$city = rgar( $entry, '10.3' );
		$state = rgar( $entry, '10.4' );
		$zip = rgar( $entry, '10.5' );
		$phone = rgar( $entry, '4' );
		$occupation = rgar( $entry, '5' );
		$employer = rgar( $entry, '6' );
		$department = rgar( $entry, '7' );
		$signature = rgar( $entry, '12');
		$signup = $su;


		$fields = 'firstname='.$firstname .'&lastname='.$lastname.'&email='.$email.'&addr1='.$addr1.'&addr2='.$addr2.'&city='.$city.'&state_cd='.$state.'&zip='.$zip.'&phone='.$phone.'&occupation='.$occupation.'&employer='.$employer.'&custom-162621='.$department.'&custom-162626='.date("F j, Y").'&custom-162627='.$signature.'&custom-176962[0]='.$signup.'&custom-181045='.$formtype;

	}
	else if($form['id'] == 6)
	{
		// share story
		//$ga_event = 'Share Story Form';
	}
	else if($form['id'] == 7 || $form['id'] == 8)
	{
		// connect with us and questions/comments
		// /faculty/connect/
		// 
		$action = "https://action.seiu.org/page/sapi/ff-questions";

		if($form['id'] == 7)
		{
			$ga_event = 'Connect Form';
		}
		else
		{
			$ga_event = 'Questions/Comments Form';
		}

		$firstname = rgar( $entry, '1.3' );
		$lastname = rgar( $entry, '1.6' );
		$email = rgar( $entry, '3' );
		$zip = rgar( $entry, '4' );
		$phone = rgar( $entry, '2' );
		$message = rgar( $entry, '5' );
		if($form['id'] == 7)
			$talkto = rgar( $entry, '6' );
		else
			$talkto = '1083525';

		//1083525 faculty member
		//1083526 grad student worker
		//1083527 organizer

		$fields = 'firstname='.$firstname .'&lastname='.$lastname.'&email='.$email.'&zip='.$zip.'&phone='.$phone.'&custom-180924='.$message.'&custom-181042='.$talkto;
	}
	else if($form['id'] == 9)
	{
		// join us (mailing list) form - short version
		$action = "https://action.seiu.org/page/sapi/connect-with-FF";
		$ga_event = 'Mailing List (short form)';

		$firstname = rgar( $entry, '3.3' );
		$lastname = rgar( $entry, '3.6' );
		$email = rgar( $entry, '2' );
		$zip = rgar( $entry, '8' );
		$department = rgar( $entry, '7' );

		$fields = 'firstname='.$firstname .'&lastname='.$lastname.'&email='.$email.'&zip='.$zip.'&custom-181046='.$department;
	}

	if($action)
	{
		$response = bsd_curl($action, $fields);
		
		$filename = $_SERVER['DOCUMENT_ROOT']."/wp-content/uploads/forms/bsd-" . $form['id'] . "-" . date("Y-m-d-H-i-s") . ".txt";
		$temp = fopen($filename, "w");
		fwrite($temp, $response);
		fclose($temp);
		

		if($response)
		{
			ga_send_event($ga_event.' Submitted', 'click', '');
		}

		return $response;		
	}

	return false;
}

function bsd_curl($action, $fields){
	$curl_handle = curl_init();

	curl_setopt($curl_handle, CURLOPT_URL, $action);
	curl_setopt($curl_handle, CURLOPT_POST, 1);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields);

	// in real life you should use something like:
	// curl_setopt($ch, CURLOPT_POSTFIELDS, 
	//          http_build_query(array('postvar1' => 'value1')));

	// receive server response ...
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);


    $response = curl_exec($curl_handle);
    //mail('km@brink.com', 'BSD cURL Results', $humanReadableError = curl_error($curl_handle).' --- '.curl_getinfo($curl_handle, CURLINFO_HTTP_CODE));
    //echo '<pre>';var_dump($pardotApiResponse); var_dump($url); echo '</pre>';
    //die();
    if ($response === false) {
        // failure - a timeout or other problem. depending on how you want to handle failures,
        // you may want to modify this code. Some folks might throw an exception here. Some might
        // log the error. May you want to return a value that signifies an error. The choice is yours!

        // let's see what went wrong -- first look at curl
        $humanReadableError = curl_error($curl_handle);

        // you can also get the HTTP response code
        $httpResponseCode = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);

        // make sure to close your handle before you bug out!
        curl_close($curl_handle);

        //throw new Exception("Unable to successfully complete Pardot API call to $url -- curl error: \""."$humanReadableError\", HTTP response code was: $httpResponseCode");

        $response = "Unable to successfully complete API call to $url -- curl error: \""."$humanReadableError\", HTTP response code was: $httpResponseCode";
    }

    // make sure to close your handle before you bug out!
    curl_close($curl_handle);

    return $response;	
}

// -------------------- google analytics server side ----------------
//Parse the GA Cookie
function gaParseCookie() {
	if (isset($_COOKIE['_ga'])) {
		list($version, $domainDepth, $cid1, $cid2) = explode('.', $_COOKIE["_ga"], 4);
		$contents = array('version' => $version, 'domainDepth' => $domainDepth, 'cid' => $cid1 . '.' . $cid2);
		$cid = $contents['cid'];
	} else {
		$cid = gaGenerateUUID();
	}
	return $cid;
}

//Generate UUID
//Special thanks to stumiller.me for this formula.
function gaGenerateUUID() {
	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		mt_rand(0, 0xffff), mt_rand(0, 0xffff),
		mt_rand(0, 0xffff),
		mt_rand(0, 0x0fff) | 0x4000,
		mt_rand(0, 0x3fff) | 0x8000,
		mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	);
}

//Send Data to Google Analytics
//https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#event
function gaSendData($data) {
	$getString = 'https://ssl.google-analytics.com/collect';
	$getString .= '?payload_data&';
	$getString .= http_build_query($data);
	$result = wp_remote_get($getString);
	return $result;
}

//Send Event Function for Server-Side Google Analytics
function ga_send_event($category=null, $action=null, $label=null) {
	$data = array(
		'v' => 1,
		'tid' => 'UA-56807821-24',
		'cid' => gaParseCookie(),
		't' => 'event',
		'ec' => $category, 
		'ea' => $action, 
		'el' => $label
	);
	gaSendData($data);
}

function new_excerpt_length($length) {
    return 40;
}
add_filter('excerpt_length', 'new_excerpt_length');

add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
	return  '/wp-content/themes/safety/public_assets/images/ajax-loader.svg';
}