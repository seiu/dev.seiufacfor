<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package seiu
 */

get_header(); ?>

<main class="site-content" role="main">

	<?php while ( have_posts() ) : the_post(); 
	
		$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
		if($image):
	?>

	<div class="hero" style="background:url(<?php echo $image[0]; ?>) center center; background-size: cover;">
		<h1><?php the_title(); ?></h1>
	</div>

	<?php endif; ?>

		

		<section class="news">
			<div class="news-list match left-side">
				<div class="container content-margin-fixer">
					<?php if(!$image): ?>
					<h1><?php the_title(); ?></h1>
					<?php endif; ?>

					<?php the_content(); ?>

					<?php 
					$questions = get_field('questions');
					if($questions): ?>
					<ul class="accordion">
						<?php $i = 0; foreach($questions AS $q): ?>
						<li<?php echo $i == 0 ? ' class="active"' : ''; ?>>
							<h3><?php echo $q['question']; ?></h3>
							<div class="answer">
								<?php echo $q['answer']; ?>
							</div>
						</li>
						<?php $i ++; endforeach; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		<?php 
		if(get_field('include_social_media_feeds') != 'yes')
		{
			$hide_social = true;
		}

		$sidebar_image = get_field('sidebar_image');
		$sidebar_cta_label = get_field('sidebar_cta_label');
		$sidebar_cta_link_url = get_field('sidebar_cta_link_url');

		include('content-sidebar.php'); ?>
		</section>

	<?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>