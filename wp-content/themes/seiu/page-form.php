<?php
/**
 * Template Name: Form Page
 *
 * @package seiu
 */

get_header(); ?>

<main class="site-content" role="main">

	<?php while ( have_posts() ) : the_post(); 
	
		$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
		if($image):
	?>

		<div class="hero" style="background:url(<?php echo $image[0]; ?>) center center; background-size: cover;">
			<h1><?php the_title(); ?></h1>
		</div>

	<?php endif; ?>

		<section class="form-page">
			<div class="container">
				<div class="form-left">
					<?php if(!$image): ?>
					<h1><?php the_title(); ?></h1>
					<?php endif; ?>

					<?php the_content(); ?>
				</div>
				<div class="form-right">
					<?php 
					$form = get_field('form');
					echo do_shortcode($form);
					?>
				</div>
			</div>
		</section>

	<?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>