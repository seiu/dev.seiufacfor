	<div class="social-feeds match">
  		<div class="share-block">
  			<?php
  			if(isset($sidebar_image) && $sidebar_image != '')
  				$image = $sidebar_image;
  			else
  				$image = get_field('sidebar_image', 10);

  			if(!isset($sidebar_cta_label) || $sidebar_cta_label == '')
  				$sidebar_cta_label = 'Questions? Connect with Us';

  			if(!isset($sidebar_cta_link_url) || $sidebar_cta_link_url == '')
  				$sidebar_cta_link_url = get_permalink(838);
  			?>
  			<img src="<?php echo $image; ?>" alt="Connect with us">
  			<a href="<?php echo $sidebar_cta_link_url; ?>" class="blue-button"><?php echo $sidebar_cta_label; ?></a>
  		</div>
  		<?php if(!isset($hide_social)): ?>
  		<div class="feed-block">
				<div class="margin-fixer">
						<div class="facebook-feed">
							<a href="https://www.facebook.com/seiufacultyforward" class="blue-button"><span class="icon fb"></span>Like Us on Facebook</a>
							<?php echo do_shortcode('[custom-facebook-feed height=400px]');?>
						</div>
				</div>
				<div class="margin-fixer">
					<div class="twitter-feed">
						<a href="https://twitter.com/FacultyForward" class="blue-button"><span class="icon tw"></span>Follow Us on Twitter</a>
						<div class="twitter-box">
							<a class="twitter-timeline" data-height="400" href="https://twitter.com/FacultyForward">Tweets by FacultyForward</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
  		</div>
  		<?php endif; ?>
		<div class="faq-cta">
			<h3>Learn more on our <a href="/frequently-asked-questions/">FAQ page</a></h3>
		</div>

		<?php if($victories): ?>
		<div class="feed-block">
			<div class="margin-fixer">
				<?php echo do_shortcode('[timeline titleonly="true"]'); ?>
			</div>
		</div>
		<?php endif; ?>
  </div>