<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package seiu
 */

get_header(); ?>

<main class="site-content" role="main">
	<section class="news">
		<div class="news-list match left-side">
			<div class="container content-margin-fixer">
				<header class="page-header">
					<h1 class="page-title">
						<?php printf( __( 'Search Results for: %s', 'seiu' ), '<span>' . get_search_query() . '</span>' ); ?>
					</h1>
				</header><!-- .page-header -->
			<?php if ( have_posts() ) : ?>
				<?php 
					while ( have_posts() ) : the_post(); 
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
				?>
					<div class="news-item">
						<?php if($image): ?>
							<div class="image" style="background:url(<?php echo $image[0]; ?>) no-repeat center center; background-size:cover;"><a href="<?php echo get_permalink($post->ID); ?>" class="overlink"></a></div>
							<?php endif; ?>
						<div class="news-blurb <?php echo $image ? '' : 'no-image'; ?>">
							<div class="date"><?php echo date("F j, Y", strtotime($post->post_date)); ?></div>
							<h2><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
							<p><?php echo seiu_truncate(strip_tags($post->post_content), 140); ?></p>
							<a href="<?php echo get_permalink($post->ID); ?>" class="read-more">Read More</a>
						</div>
					</div>
				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'search' ); ?>

			<?php endif; ?>
			</div>
		</div>
	<?php include('content-sidebar.php'); ?>
	</section>
</main><!-- #primary -->

<?php get_footer(); ?>